import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

export class FoundPatterns extends Array {

    constructor() {
        super();
    }

    getFalsePositives(groundTruths){

        let falsePos = [];

        this.forEach((found)=>{

            let foundMatch = false;

            groundTruths.forEach((truth) => {
                //If the start or end of the found pattern is within the truth, it is a match, so not a false positive
                if((found.start >= truth.start && found.start <= truth.end) ||
                    (found.end >= truth.start && found.end <= truth.end) ||
                    (found.start <= truth.start && found.end >= truth.end)){
                    foundMatch = true;
                }
            })

            //If no match has been found, the found pattern is a false positive.
            if(!foundMatch){
                falsePos.push(found);
            }
        })

        return falsePos;

    }

    getFoundPattern(start, end){
        let result;
        this.forEach((pattern) =>{
            if(pattern.start === start && pattern.end === end){
                result = pattern;
            }
        })
        return result;
    }
}