import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

export class ScatterPlotter{

    glyphCharter;

    parent;
    height;
    width;
    glyphList;

    IDGlyphList;

    xScale;
    yScale;

    scoringScaleExcess;

    colorScale;

    tooltip;

    dots;

    constructor(parent, height, width) {

        this.parent = parent;
        this.height = height;
        //Take 100 off width to create space for the legend for the color coding
        this.width = width - 100;

        this.tooltip = d3.select("#scatterPlotTooltipDiv");
        this.drawLegend();
    }

    setGlyphCharter(glyphCharter){
        this.glyphCharter = glyphCharter;
    }

    draw(){
        //First, set all scales
        this.generateScales();

        //Generate axis with the scales
        let xAxis = this.parent.append("g")
            .attr("transform", "translate(0," + this.height + ")").call(d3.axisBottom(this.xScale));
        let yAxis = this.parent.append("g").call(d3.axisRight(this.yScale));

        //Label the axis
        this.parent.append("text")
            .attr("text-anchor", "end")
            .attr("x", this.width)
            .attr("y", this.height + 30)
            .text("Ground Truth Coverage Percentage");

        this.parent.append("text")
            .attr("text-anchor", "end")
            .attr("transform", "rotate(-90)")
            .attr("y", -10)
            .attr("x", 0)
            .text("Excess")

        //Draw scatterplot
        this.dots = this.parent.append("g")
            .selectAll("dot")
            .data(this.getTruePositives(this.glyphList))
            .enter()
            .append("circle")
            .attr("class", "glyphDot")
            .attr("cx", d => {return this.xScale(d.getGTCoveragePercent())})
            .attr("cy", d => {return this.yScale(d.getTotalExcess())})
            .attr("r",  5)
            .style("fill", d => {
                let scoreCoverage = d.getGTCoveragePercent();
                let scoreExcess = this.scoringScaleExcess(d.getTotalExcess());
                return this.colorScale(scoreCoverage[0] + scoreExcess);
            })
            .on("mouseover", (d,i) =>{
                let scoreCoverage = i.getGTCoveragePercent();
                let scoreExcess = this.scoringScaleExcess(i.getTotalExcess());

                this.tooltip.style("visibility", "visible").append("p").html(
                    "Glyph Identifier: " + this.IDGlyphList.indexOf(i) + "<br>" +
                    "Coverage Percentage: " + Math.round((+i.getGTCoveragePercent() + Number.EPSILON) * 100)/100 + "<br>" +
                    "Excess: " + Math.round((i.getTotalExcess() + Number.EPSILON) * 100)/100 + "<br>" +
                    "Score: " + Math.round(((+scoreCoverage + +scoreExcess) + Number.EPSILON) * 100)/100
                )
            })
            .on("mousemove", d =>{
                let yOffset = this.tooltip.node().getBoundingClientRect().height / 2;
                this.tooltip.style("top", ((event.pageY) - yOffset) + "px").style("left", (event.pageX) + 15 + "px");
            })
            .on("mouseout", d =>{
                d3.selectAll("#scatterPlotTooltipDiv > *").remove();
                this.tooltip.style("visibility", "hidden");
            });

        //this rectangle will be used to create a selection
        let selectionRect = this.parent.append("rect")
            .attr("id", "selectionrect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", 0)
            .attr("height", 0)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("visibility", "hidden");

        //Here we add the functionality for drawing a selection box
        //First variables track dragging
        let dragging = false;
        let startX = 0;
        let startY = 0;
        d3.select("#scatterPlotTarget > svg")
            .on("mousedown", d=>{
                dragging = true;
                selectionRect.attr("visibility", "visible")
                    .attr("x", d.offsetX - 30)
                    .attr("y", d.offsetY - 20)
                    .attr("width", 0)
                    .attr("height", 0);
                startX = d.offsetX;
                startY = d.offsetY;
            }).on("mousemove", d=> {
                if(dragging){
                    if(((d.offsetX - startX) >= 0) && ((d.offsetY - startY) >= 0)){
                        selectionRect.attr("width", d.offsetX - startX)
                            .attr("height", d.offsetY - startY);
                    } else if (((d.offsetX - startX) < 0) && ((d.offsetY - startY) < 0)){
                        selectionRect.attr("x", d.offsetX - 35/2)
                            .attr("y", d.offsetY - 30/2)
                            .attr("width", startX - d.offsetX)
                            .attr("height", startY - d.offsetY);
                    } else if(((d.offsetX - startX) < 0) && ((d.offsetY - startY) >= 0)){
                        selectionRect.attr("x", d.offsetX - 35/2)
                            .attr("width", startX - d.offsetX)
                            .attr("height", d.offsetY - startY);
                    } else if(((d.offsetX - startX) >= 0) && ((d.offsetY - startY) < 0)){
                        selectionRect.attr("y", d.offsetY - 30/2)
                            .attr("width", d.offsetX - startX)
                            .attr("height", startY - d.offsetY);
                    }
                }
            })
            .on("mouseup", d=>{
                dragging = false;
                //Here we call the functions in the glyphcharter for the selection and for finding what points are
                //in the selection

                console.log(d.offsetX < this.width);

                this.getDotsByArea(+selectionRect.attr("x"), +selectionRect.attr("y"),
                    +selectionRect.attr("width"), +selectionRect.attr("height"));
            })
    }

    getDotsByArea(x, y, width, height){

        let selection = [];

        this.dots.each(function(d){
            let cx = d3.select(this).attr("cx");
            let cy = d3.select(this).attr("cy");

            if(((cx >= x)&&(cx <= x+width))&&((cy >= y)&&(cy <= y+height))){
                selection.push(d);
            }
        })

        console.log(selection);
        this.glyphCharter.glyphList = selection;
        this.glyphCharter.updatePagination();
        this.glyphCharter.draw();
    }

    getTruePositives(glyphList){
        let result = [];
        glyphList.forEach((glyph) => {
            if(glyph.foundPatterns.length > 0 && glyph.groundTruth !== null){
                result.push(glyph);
            }
        })
        return result;
    }

    setGlyphList(glyphList){
        this.glyphList = glyphList;
        this.IDGlyphList = glyphList;
    }

    generateScales(){
        //xScale currently shows the coverage percentage, yScale the amount of excess
        this.xScale = d3.scaleLinear()
            .domain([0,100])
            .range([0, this.width]);

        //Generate excess List for the purpose of creating scales
        let excessList = [];

        this.glyphList.forEach((glyph) => {
            if(glyph.getExcess().length > 0){
                let excess = glyph.getExcess()[0];
                let addedExcess = 0;
                if(excess[0] >= 0){
                    addedExcess += excess[0];
                }
                if(excess[1] >= 0){
                    addedExcess += excess[1];
                }
                excessList.push(addedExcess);
            }
        })

        this.yScale = d3.scaleLinear()
            .domain([d3.min(excessList), d3.max(excessList)])
            .range([this.height, 0]);

        //Generate scoring scale as well. This scale scores a glyph from 0-100 according to its excess.
        this.scoringScaleExcess = d3.scaleLinear()
            .domain([d3.min(excessList), d3.max(excessList)])
            .range([100,0]);

        //Generate color scale
        this.colorScale = d3.scaleLinear()
            .domain([0,200])
            .range(["red", "green"]);
    }

    updateIDList(glyphList){
        this.IDGlyphList = glyphList;
    }

    drawLegend(){
        let colors = ["red", "green"];

        let grad = this.parent.append("defs")
            .append("linearGradient")
            .attr("id", "grad")
            .attr("x1", "0%")
            .attr("x2", "0%")
            .attr("y1", "0%")
            .attr("y2", "100%");

        grad.selectAll("stop")
            .data(colors)
            .enter()
            .append("stop")
            .style("stop-color", function(d){return d;})
            .attr("offset", function(d,i){
                return 100 * (i/(colors.length - 1)) + "%";
            })

        this.parent.append("text")
            .attr("text-anchor", "middle")
            .attr("x", this.width + 50)
            .attr("y", (this.height - 210) / 2 - 20)
            .text("Score:")

        this.parent.append("rect")
            .attr("x", this.width + 40)
            .attr("y", (this.height - 200) / 2)
            .attr("width", 20)
            .attr("height", 200)
            .style("fill", "url(#grad)");

        let height = this.height;

        this.parent.append("line")
            .attr("x1", this.width + 40)
            .attr("x2", this.width + 65)
            .attr("y1", (this.height-200) / 2)
            .attr("y2", (this.height-200) / 2)
            .attr("stroke", "black");

        this.parent.append("text")
            .text("0")
            .attr("x", this.width + 70)
            .attr("y", function(){
                console.log(d3.select(this).style("font-size").slice(0,-2))
                return ((height - 200) / 2) + 6;
            });

        this.parent.append("line")
            .attr("x1", this.width + 40)
            .attr("x2", this.width + 65)
            .attr("y1", (this.height-200) / 2 + 200)
            .attr("y2", (this.height-200) / 2 + 200)
            .attr("stroke", "black");

        this.parent.append("text")
            .text("200")
            .attr("x", this.width + 70)
            .attr("y", ((height-200) / 2) + 200 + 5);


        //Resetbutton gets added here as well
        this.parent.append("foreignObject")
            .attr("x", this.width + 20)
            .attr("y", (this.height-200) / 2 + 230)
            .attr("width", 100)
            .attr("height", 100)
            .append("xhtml:button")
            .attr("class", "button")
            .text("Reset")
            .on("click", d => {
                let sorting = this.glyphCharter.sorting;
                this.glyphCharter.generateGlyphList();
                this.glyphCharter.sortGlyphs(sorting);
                this.glyphCharter.updatePagination();
                this.glyphCharter.draw();
            })
    }
}