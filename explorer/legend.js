import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

export class Legender{
    parent;
    width;
    height;
    foundpatterncolor;
    seriescolor;
    groundtruthcolor;

    constructor(parent, width, height, foundpatterncolor, seriescolor, groundtruthcolor) {
        this.parent = parent;
        this.width = width;
        this.height = height;
        this.padding = 20;

        this.foundpatterncolor = foundpatterncolor;
        this.seriescolor = seriescolor;
        this.groundtruthcolor = groundtruthcolor;

        this.draw();
    }

    draw(){
        this.parent.append("rect")
            .attr("id", "legendGroundTruth")
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", ((this.height/3)-this.padding))
            .attr("width", 50)
            .attr("stroke","none")
            .attr("fill", this.groundtruthcolor);

        this.parent.append("text")
            .attr("id", "legendGroundText")
            .attr("x", 50 + this.padding)
            .attr("y", 17)
            .attr("font-family", "Verdana")
            .attr("font-size", 14)
            .text("Pre-determined Ground Truth");

        this.parent.append("rect")
            .attr("id", "legendFoundPattern")
            .attr("x", 0)
            .attr("y", ((this.height/3)-this.padding) + this.padding)
            .attr("height", ((this.height/3)-this.padding))
            .attr("width", 50)
            .attr("stroke","none")
            .attr("fill", this.foundpatterncolor);

        this.parent.append("text")
            .attr("id", "legendGroundText")
            .attr("x", 50 + this.padding)
            .attr("y", 17 + (((this.height/3)-this.padding) + this.padding))
            .attr("font-family", "Verdana")
            .attr("font-size", 14)
            .text("Generated Found Pattern");

        this.parent.append("rect")
            .attr("id", "legendOverlap")
            .attr("x", 0)
            .attr("y", 2*(((this.height/3)-this.padding) + this.padding))
            .attr("height", ((this.height/3)-this.padding))
            .attr("width", 50)
            .attr("stroke","none")
            .attr("fill", "url(#diagonalHatch)");

        this.parent.append("text")
            .attr("id", "legendGroundText")
            .attr("x", 50 + this.padding)
            .attr("y", 17 + (2*(((this.height/3)-this.padding) + this.padding)))
            .attr("font-family", "Verdana")
            .attr("font-size", 14)
            .text("Matching Area between Ground Truth and Found Pattern");
    }
}