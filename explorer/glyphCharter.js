import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";
import {Glyph} from "/explorer/glyph.js";

export class GlyphCharter{

    parent;
    width;
    height;
    xScale;
    yScale;
    shadowlineScale;
    arcScale;
    timeSeriesScale;
    groundTruths;
    foundPatterns;
    timeSeries;
    confidenceCutoff;
    glyphList;
    sorting;
    showShadowLines;
    shadowLineTrack;

    //At this point these are set within the class, to be change later
    currentPage;
    glyphsperpage;
    pages;

    drawMethod = "bars";

    //Linegrapher, used for zooming
    lineGrapher;

    //Scatterplotter, creates scatterplot of glyphs
    scatterPlotter;

    //Keep track if IDS are shown
    idShown = true;

    constructor(parent, width, height, groundTruths, foundPatterns, timeSeries, confidenceCutoff = 0.5,
                lineGrapher, scatterPlotter) {
        this.parent = parent;
        this.width = width;
        this.height = height;
        this.groundTruths = groundTruths;
        this.foundPatterns = foundPatterns;
        this.timeSeries = timeSeries;
        this.confidenceCutoff = confidenceCutoff;
        //Initial sorting is confidence descending
        this.orderedDisplay = [];

        this.lineGrapher = lineGrapher;

        this.showShadowLines = true;
        //TODO change to dropdown later
        this.shadowLineTrack = timeSeries.columns[1];

        //Set variables for pagination, to be changed to more pretty location
        this.currentPage = 1;
        this.glyphsperpage = 12;

        //Setup initial scales
        //TODO remove hardcoded min and max
        this.xScale = d3.scaleLinear();
        //The amount of patterns displayed vertically is equal to the amount of ground truths + the amount of false positives
        //TODO add in pagination
        // this.yScale = d3.scaleLinear()
        //     .domain([0, (this.groundTruths.length +
        //         this.foundPatterns.getFalsePositives(this.groundTruths).length)-1])
        //     .range([this.height - 20, 0]);
        this.yScale = d3.scaleLinear()
            .domain([0, this.glyphsperpage])
            .range([0, this.height]);

        this.arcScale = d3.scaleLinear()
            .domain([0, 1])
            .range([0, 2 * Math.PI]);

        this.shadowlineScale = d3.scaleLinear()
            .domain([d3.min(this.timeSeries, d => {
                return d[this.shadowLineTrack]
            }), d3.max(this.timeSeries, d => {
                return d[this.shadowLineTrack]
            })]);

        //Values and scale for converting smaller timesteps to indices
        let timeMin = d3.min(timeSeries, d => {
            return d.Time
        });
        let timeMax = d3.max(timeSeries, d => {
            return d.Time
        });
        let seriesLength = timeSeries.length;
        this.timeSeriesScale = d3.scaleLinear()
            .domain([timeMin, timeMax])
            .range([0, seriesLength]);

        //Populate glyphList according to cutoff
        this.generateGlyphList();

        //After generating initial glyphList, hand this off to the scatterplot
        this.scatterPlotter = scatterPlotter;
        this.scatterPlotter.setGlyphCharter(this);
        this.scatterPlotter.setGlyphList(this.glyphList);
        this.scatterPlotter.draw();

        //Split the viz: 2/3 glyphs, 1/3 confidence visualization
        let largestLength = 0;
        this.glyphList.forEach((glyph) => {
            let currentLength = glyph.getLargestEnd() - glyph.getSmallestStart();
            if (currentLength > largestLength) {
                largestLength = currentLength;
            }
        });

        //Add options to glyphtrackselector
        this.initTrackSelectionSelect();

        this.xScale.domain([0, largestLength])
            .range([0, (this.width / 3 * 2)-30])

        //Calculate needed pages for all glyphs
        this.pages = Math.ceil(this.glyphList.length / this.glyphsperpage);

        //Initial sorting and drawing
        this.updateSorting("chronAsc");

        //Initialize pagination
        this.initPagination();

        //Initialize tooltip and add hover to rectangles
        this.initTooltip();
    }

    draw(){
        //Set table header text:
        d3.select("#confidenceThresholdHeader")
            .text("Current Confidence Cutoff: " + this.confidenceCutoff);

        //Clear the visualization before drawing
        d3.selectAll("#glyphChartTarget > svg > g > *").remove();

        this.shadowlineScale.range([0, (this.yScale(1-0.5) - this.yScale(1))]);

        let shadowlineScale = this.shadowlineScale;

        let currentPageList = this.getCurrentPageGlyphs();

        currentPageList.forEach((glyph, index) =>{

            glyph.getGTCoveragePercent();

            //TODO make prettier
            if(this.idShown){
                let x1 = this.width/3*2 -15;

                this.parent.append("circle")
                    .attr("cx", x1+5)
                    .attr("cy", this.yScale(index)+15)
                    .attr("r", (this.yScale(index)-this.yScale(index-0.5))/2)
                    .attr("fill", "none")
                    .attr("stroke", "black");

                this.parent.append("text")
                    .attr("x", d => {
                        if(this.glyphList.indexOf(glyph).toString().length === 1){
                            return x1;
                        } else if(this.glyphList.indexOf(glyph).toString().length === 2){
                            return x1 - 5;
                        } else {
                            return x1 - 10;
                        }
                    })
                    .attr("y",this.yScale(index) + 20)
                    .attr("stroke", "black")
                    .text(this.glyphList.indexOf(glyph));

                this.lineGrapher.drawIds(this.glyphList);
                this.scatterPlotter.updateIDList(this.glyphList);
            }

            //If statement, has all cases --> True positive, false Negative, false Positive
            if(glyph.groundTruth !== null && glyph.foundPatterns.length !== 0){
                //Case: True Positive, Draw ground truth, found pattern and overlap (in that order). End with arc and text detailing confidence

                this.parent.append("rect")
                    .attr("class", "truthRect")
                    .attr("fill", glyph.groundTruth.color)
                    .attr("stroke", "none")
                    .attr("x", this.xScale(glyph.groundTruth.start) - this.xScale(glyph.getSmallestStart()))
                    .attr("y", this.yScale(index))
                    .attr("width", this.xScale(glyph.groundTruth.end) - this.xScale(glyph.groundTruth.start))
                    .attr("height", this.yScale(index) - this.yScale(index-0.5))
                    .on("click", d => {
                        if(this.lineGrapher.zoomed){
                            this.lineGrapher.updateXScale(0, d3.max(this.timeSeries, d => {return d.Time}));
                            this.lineGrapher.miniMapper.changeLocation(0, d3.max(this.timeSeries, d => {return d.Time}));
                            d3.selectAll("#glyphSelectionBox").remove();
                        } else {
                            this.lineGrapher.updateXScale(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.lineGrapher.miniMapper.changeLocation(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.parent.append("rect")
                                .attr("id", "glyphSelectionBox")
                                .attr("fill", "none")
                                .attr("stroke", "black")
                                .attr("stroke-width", 0.5)
                                .attr("x", 0)
                                .attr("y", this.yScale(index))
                                .attr("width", this.xScale(glyph.getLargestEnd()) - this.xScale(glyph.getSmallestStart()))
                                .attr("height", this.yScale(index) - this.yScale(index-0.5));
                        }
                        if(this.idShown){
                            this.lineGrapher.drawIds(this.glyphList);
                        }
                    });

                let opacityScale = d3.scaleLinear()
                    .domain([0,1])
                    .range([0.5,1]);

                //TODO change color choosing for multiple patterns per truth
                this.parent.append("rect")
                    .attr("class", "foundRect")
                    .attr("fill", glyph.foundPatterns[0].color)
                    .attr("stroke", "none")
                    .attr("x", this.xScale(glyph.foundPatterns[0].start) - this.xScale(glyph.getSmallestStart()))
                    .attr("y", this.yScale(index))
                    .attr("width", this.xScale(glyph.foundPatterns[0].end) - this.xScale(glyph.foundPatterns[0].start))
                    .attr("height", this.yScale(index) - this.yScale(index-0.5))
                    .attr("opacity", opacityScale(glyph.foundPatterns[0].confidence))
                    .on("click", d => {
                        if(this.lineGrapher.zoomed){
                            this.lineGrapher.updateXScale(0, d3.max(this.timeSeries, d => {return d.Time}));
                            this.lineGrapher.miniMapper.changeLocation(0, d3.max(this.timeSeries, d => {return d.Time}));
                            d3.selectAll("#glyphSelectionBox").remove();
                        } else {
                            this.lineGrapher.updateXScale(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.lineGrapher.miniMapper.changeLocation(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.parent.append("rect")
                                .attr("id", "glyphSelectionBox")
                                .attr("fill", "none")
                                .attr("stroke", "black")
                                .attr("stroke-width", 0.5)
                                .attr("x", 0)
                                .attr("y", this.yScale(index))
                                .attr("width", this.xScale(glyph.getLargestEnd()) - this.xScale(glyph.getSmallestStart()))
                                .attr("height", this.yScale(index) - this.yScale(index-0.5));
                        }
                        if(this.idShown){
                            this.lineGrapher.drawIds(this.glyphList);
                        }
                    });

                //TODO handle more patterns in the long run
                let overlap = glyph.getOverlap()[0];

                this.parent.append("rect")
                    .attr("class", "overlapRect")
                    .style("fill", "url(#diagonalHatch)")
                    .attr("stroke", "none")
                    .attr("x", this.xScale(overlap[0]) - this.xScale(glyph.getSmallestStart()))
                    .attr("y", this.yScale(index))
                    .attr("width", this.xScale(overlap[1]) - this.xScale(overlap[0]))
                    .attr("height", this.yScale(index) - this.yScale(index-0.5))
                    .attr("opacity", opacityScale(glyph.foundPatterns[0].confidence))
                    .on("click", d => {
                        if(this.lineGrapher.zoomed){
                            this.lineGrapher.updateXScale(0, d3.max(this.timeSeries, d => {return d.Time}));
                            this.lineGrapher.miniMapper.changeLocation(0, d3.max(this.timeSeries, d => {return d.Time}));
                            d3.selectAll("#glyphSelectionBox").remove();
                        } else {
                            this.lineGrapher.updateXScale(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.lineGrapher.miniMapper.changeLocation(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.parent.append("rect")
                                .attr("id", "glyphSelectionBox")
                                .attr("fill", "none")
                                .attr("stroke", "black")
                                .attr("stroke-width", 0.5)
                                .attr("x", 0)
                                .attr("y", this.yScale(index))
                                .attr("width", this.xScale(glyph.getLargestEnd()) - this.xScale(glyph.getSmallestStart()))
                                .attr("height", this.yScale(index) - this.yScale(index-0.5));
                        }
                        if(this.idShown){
                            this.lineGrapher.drawIds(this.glyphList);
                        }
                    });

                //Draw shadowed line graph behind

                let xScale = this.xScale;
                let yScale = this.yScale;

                this.parent.append("path")
                    .datum(this.getTimeSeriesSlice(this.timeSeriesScale(glyph.getSmallestStart()), this.timeSeriesScale(glyph.getLargestEnd())))
                    .attr("class", "shadowline")
                    .attr("fill", "none")
                    .attr("stroke", "black")
                    .attr("stroke-width", 1.3)
                    .attr("opacity", 0.22)
                    .attr("d", d3.line()
                        .x( (d,i) => {
                            return xScale(this.timeSeriesScale.invert(i));
                        })
                        .y(d => {
                            return shadowlineScale(d[this.shadowLineTrack]) + yScale(index+0.5);
                        }))

                //Call the method that will draw the confidence in;
                this.drawConfidence(glyph, index);
            } else if(glyph.groundTruth === null){
                //Case: False Positive --> No ground truth. Draw Rectangles and arc with text.

                this.parent.append("rect")
                    .attr("class", "foundRect")
                    .attr("fill", glyph.foundPatterns[0].color)
                    .attr("stroke", "none")
                    .attr("x", this.xScale(glyph.foundPatterns[0].start) - this.xScale(glyph.getSmallestStart()))
                    .attr("y", this.yScale(index))
                    .attr("width", this.xScale(glyph.foundPatterns[0].end) - this.xScale(glyph.foundPatterns[0].start))
                    .attr("height", this.yScale(index) - this.yScale(index-0.5))
                    .on("click", d => {
                        if(this.lineGrapher.zoomed){
                            this.lineGrapher.updateXScale(0, d3.max(this.timeSeries, d => {return d.Time}));
                            this.lineGrapher.miniMapper.changeLocation(0, d3.max(this.timeSeries, d => {return d.Time}));
                            d3.selectAll("#glyphSelectionBox").remove();
                        } else {
                            this.lineGrapher.updateXScale(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.lineGrapher.miniMapper.changeLocation(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.parent.append("rect")
                                .attr("id", "glyphSelectionBox")
                                .attr("fill", "none")
                                .attr("stroke", "black")
                                .attr("stroke-width", 0.5)
                                .attr("x", 0)
                                .attr("y", this.yScale(index))
                                .attr("width", this.xScale(glyph.getLargestEnd()) - this.xScale(glyph.getSmallestStart()))
                                .attr("height", this.yScale(index) - this.yScale(index-0.5));
                        }
                        if(this.idShown){
                            this.lineGrapher.drawIds(this.glyphList);
                        }
                    });

                //Draw shadowed line graph behind

                let xScale = this.xScale;
                let yScale = this.yScale;

                this.parent.append("path")
                    .datum(this.getTimeSeriesSlice(this.timeSeriesScale(glyph.getSmallestStart()), this.timeSeriesScale(glyph.getLargestEnd())))
                    .attr("class", "shadowline")
                    .attr("fill", "none")
                    .attr("stroke", "black")
                    .attr("stroke-width", 1.3)
                    .attr("opacity", 0.22)
                    .attr("d", d3.line()
                        .x( (d,i) => {
                            return xScale(this.timeSeriesScale.invert(i));
                        })
                        .y(d => {
                            return shadowlineScale(d[this.shadowLineTrack]) + yScale(index+0.5);
                        }))

                //Call the method that will draw the confidence in;
                this.drawConfidence(glyph, index);

            } else if(glyph.foundPatterns.length === 0){
                //Case: False Negative --> No patterns found, only draw ground truth;

                this.parent.append("rect")
                    .attr("class", "truthRect")
                    .attr("fill", glyph.groundTruth.color)
                    .attr("stroke", "none")
                    .attr("x", this.xScale(glyph.groundTruth.start) - this.xScale(glyph.getSmallestStart()))
                    .attr("y", this.yScale(index))
                    .attr("width", this.xScale(glyph.groundTruth.end) - this.xScale(glyph.groundTruth.start))
                    .attr("height", this.yScale(index) - this.yScale(index-0.5))
                    .on("click", d => {
                        if(this.lineGrapher.zoomed){
                            this.lineGrapher.updateXScale(0, d3.max(this.timeSeries, d => {return d.Time}));
                            this.lineGrapher.miniMapper.changeLocation(0, d3.max(this.timeSeries, d => {return d.Time}));
                            d3.selectAll("#glyphSelectionBox").remove();
                        } else {
                            this.lineGrapher.updateXScale(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.lineGrapher.miniMapper.changeLocation(glyph.getSmallestStart(), glyph.getLargestEnd());
                            this.parent.append("rect")
                                .attr("id", "glyphSelectionBox")
                                .attr("fill", "none")
                                .attr("stroke", "black")
                                .attr("stroke-width", 0.5)
                                .attr("x", 0)
                                .attr("y", this.yScale(index))
                                .attr("width", this.xScale(glyph.getLargestEnd()) - this.xScale(glyph.getSmallestStart()))
                                .attr("height", this.yScale(index) - this.yScale(index-0.5));
                        }
                        if(this.idShown){
                            this.lineGrapher.drawIds(this.glyphList);
                        }
                    });


                //Draw shadowed line graph behind

                let xScale = this.xScale;
                let yScale = this.yScale;

                this.parent.append("path")
                    .datum(this.getTimeSeriesSlice(this.timeSeriesScale(glyph.getSmallestStart()), this.timeSeriesScale(glyph.getLargestEnd())))
                    .attr("class", "shadowline")
                    .attr("fill", "none")
                    .attr("stroke", "black")
                    .attr("stroke-width", 1.3)
                    .attr("opacity", 0.22)
                    .attr("d", d3.line()
                        .x( (d,i) => {
                            return xScale(this.timeSeriesScale.invert(i));
                        })
                        .y(d => {
                            return shadowlineScale(d[this.shadowLineTrack]) + yScale(index+0.5);
                        }))
            }
        })

        //Init tooltip after drawing again, otherwise it only works once.
        this.initTooltip();

        this.setShadowLines();
    }

    resetZoomLineGraph(){
        this.lineGrapher.resetZoom();
        if(this.idShown){
            this.lineGrapher.drawIds(this.glyphList);
        }
    }

    miniMapZoomLineGraph(start, end){
        this.lineGrapher.miniMapZoom(start, end);
        if(this.idShown){
            this.lineGrapher.drawIds(this.glyphList);
        }
    }

    toggleIDS(){
        let largestLength = 0;
        this.glyphList.forEach((glyph) => {
            let currentLength = glyph.getLargestEnd() - glyph.getSmallestStart();
            if (currentLength > largestLength) {
                largestLength = currentLength;
            }
        });
        if(this.idShown){
            this.xScale.domain([]).domain([0, largestLength])
                .range([0, (this.width / 3 * 2)]);
            d3.selectAll(".lineGraphIDText").remove();
            d3.selectAll(".lineGraphIDCircle").remove();
        } else {
            this.xScale.domain([]).domain([0, largestLength])
                .range([0, (this.width / 3 * 2) - 30]);
            this.lineGrapher.drawIds(this.glyphList);
        }
        this.idShown = !this.idShown;

        this.generateGlyphList();
        this.sortGlyphs();
        this.draw();
    }

    generateConfidenceBars(){

        this.glyphList.forEach((glyph) => {
            if(glyph.foundPatterns !== undefined && glyph.foundPatterns.length > 0){
                d3.select("#confidenceBarTarget").append("progress")
                    .attr("class", "progress is-primary")
                    .attr("value", Math.round((+glyph.foundPatterns[0].confidence + Number.EPSILON) * 100)/100)
                    .attr("max", 1);
            }
        })

    }

    toggleDrawMethod(){
        if(this.drawMethod === "arcs"){
            this.drawMethod = "bars";
        } else {
            this.drawMethod = "arcs";
        }

        this.generateGlyphList();
        this.sortGlyphs();
        this.draw();
    }

    drawConfidence(glyph, index){
        let drawMethod = this.drawMethod;

        if(drawMethod === "arcs"){
            //Draw angles to show confidence

            let backgroundArcAngles = [{startAngle: 0, endAngle: 2 * Math.PI}];
            let backgroundArc = d3.arc()
                .innerRadius(10)
                .outerRadius(15)
                .cornerRadius(5);
            let backgroundArcPath = this.parent.append("g")
                .selectAll("path.arc")
                .data(backgroundArcAngles);
            backgroundArcPath.enter()
                .append("path")
                .attr("transform", "translate(" + (this.width) + "," + (this.yScale(index) + 20) + ")") //Plus 20 for margin top
                .attr("class", "backgroundarc")
                .attr("fill", "lightgray")
                .attr("d", backgroundArc);

            let dataArcAngles = [{startAngle: 0, endAngle: this.arcScale(glyph.foundPatterns[0].confidence)}];
            let dataArc = d3.arc()
                .innerRadius(10)
                .outerRadius(15)
                .cornerRadius(5);
            let dataArcPath = this.parent.append("g")
                .selectAll("path.arc")
                .data(dataArcAngles);
            dataArcPath.enter()
                .append("path")
                .attr("transform", "translate(" + (this.width) + "," + (this.yScale(index) + 20) + ")")
                .attr("class", "dataarc")
                .attr("fill", "#FFE6AB")
                .attr("d", dataArc);

            this.parent.append("text")
                .attr("x", this.width-9)
                .attr("y", this.yScale(index) + 23)
                .attr("font-family", "Verdana")
                .attr("font-size", 8)
                .text(Math.round((+glyph.foundPatterns[0].confidence + Number.EPSILON) * 100)/100);

        } else if (drawMethod === "bars"){

            this.parent.append("foreignObject")
                .attr("x", this.width/3*2+25)
                .attr("y", this.yScale(index))
                .attr("height", this.yScale(index) - this.yScale(index-0.5))
                .attr("width", this.width/3)
                .append("xhtml:div")
                .attr("class", "progress-container")
                .attr("data-text", Math.round((+glyph.foundPatterns[0].confidence + Number.EPSILON) * 100)/100)
                .style("display", "flex")
                .style("align-items", "center")
                .style("justify-content", "center")
                .style("height", "inherit")
                .append("xhtml:progress")
                .style("margin", "0")
                .attr("class", "progress is-primary")
                .attr("value", glyph.foundPatterns[0].confidence)
                .attr("max", 1);
        }
    }

    sortGlyphs(){
        let foundPatterns = this.foundPatterns;
        let sortStyle = this.sorting;
        //Sort the viz by different styles


        //Chronological sort, first glyph comes first
        if(sortStyle === "confAsc"){
            this.glyphList = this.glyphList.sort(function(a,b){
                //First if statement bubbles the false negatives (no confidence) to the back. Negate both return to send them to front.
                if(a.getAverageConf() === null){
                    return 1;
                } else if (b.getAverageConf() === null){
                    return -1;
                } else {
                    return a.getAverageConf() - b.getAverageConf();
                }
            })
        } else if (sortStyle === "confDesc") {
            this.glyphList = this.glyphList.sort(function(a,b){
                //First if statement bubbles the false negatives (no confidence) to the back. Negate both return to send them to front.
                if(a.getAverageConf() === null){
                    return 1;
                } else if (b.getAverageConf() === null){
                    return -1;
                } else {
                    return b.getAverageConf() - a.getAverageConf();
                }
            })
        } else if (sortStyle === "chronAsc") {
            this.glyphList = this.glyphList.sort(function(a,b){
                //First two cases check if a ground truth exists (doesn't in the case of false positive), if it doesn't it compares the found patterns start
                if(a.groundTruth === null && b.groundTruth === null){
                    return a.getSmallestFoundStart() - b.getSmallestFoundStart();
                } else if(a.groundTruth === null){
                    return a.getSmallestFoundStart() - b.groundTruth.start;
                } else if(b.groundTruth === null){
                    return a.groundTruth.start - b.getSmallestFoundStart();
                } else {
                    return a.groundTruth.start - b.groundTruth.start;
                }
            })
        } else if (sortStyle === "chronDesc") {
            this.glyphList = this.glyphList.sort(function(a,b){
                //Check if both

                //First two cases check if a ground truth exists (doesn't in the case of false positive), if it doesn't it compares the found patterns start
                if(a.groundTruth === null && b.groundTruth === null){
                    return b.getSmallestFoundStart() - a.getSmallestFoundStart();
                } else if(b.groundTruth === null){
                    return b.getSmallestFoundStart() - a.groundTruth.start;
                } else if(a.groundTruth === null){
                    return b.groundTruth.start - a.getSmallestFoundStart();
                } else {
                    return b.groundTruth.start - a.groundTruth.start;
                }
            })
        }
    }

    generateGlyphList(){
        this.glyphList = [];

        let confidenceCutoff = this.confidenceCutoff;

        this.groundTruths.forEach((truth) => {
            //Get all patterns matching the truth
            let matches = truth.getMatches(this.foundPatterns);

            //Only keep the found patterns with high enough confidence
            matches = matches.filter((match) => {
                return match.confidence>=confidenceCutoff});

            //Check for empty matches, then add an empty list
            if(matches.length > 0){
                matches.forEach((match) => {
                    this.glyphList.push(new Glyph(truth, [match]));
                })
            } else {
                this.glyphList.push(new Glyph(truth, matches));
            }
        })

        //Add glyphs for false positives (false negatives added in the last loop)
        this.foundPatterns.getFalsePositives(this.groundTruths).forEach((falsePos) => {
            if(falsePos.confidence >= confidenceCutoff){
                this.glyphList.push(new Glyph(null, [falsePos]));
            }
        })
    }

    updateSorting(sortStyle){
        this.sorting = sortStyle;
        this.sortGlyphs();
        this.lineGrapher.drawIds(this.glyphList);
        this.draw();
    }

    setConfidenceCutoff(newCutoff){
        this.confidenceCutoff = newCutoff;
        this.generateGlyphList();
        this.sortGlyphs();


        //If you started on a page which no longer exists after filtering, go to the last remaining page.
        if(this.currentPage * this.glyphsperpage > this.glyphList.length){
            this.changePage(Math.ceil(this.glyphList.length / this.glyphsperpage));
            this.updatePagination();
        } else {//Change pagination to disable buttons that are not needed after cutoff
            this.updatePagination();

            this.draw();
        }
    }
    getCurrentConfidenceCutoff(){
        return this.confidenceCutoff;
    }

    updatePagination(){
        let newPages = Math.ceil(this.glyphList.length / this.glyphsperpage);

        let paginationNumbers = d3.selectAll(".pagination-link").attr("disabled", null);
        paginationNumbers.each(function(d){
            if((d3.select(this).attr("id")).at(-1) > newPages){
                d3.select(this).attr("disabled", "disabled");
            }
        })

        if(this.currentPage * this.glyphsperpage > this.glyphList.length){
            d3.select("#paginationNext").attr("disabled", "disabled");
        } else {
            d3.select("#paginationNext").attr("disabled", null);
        }
    }

    //TODO change tooltip for multiple found patterns per ground truth
    initTooltip() {
        let yScale = this.yScale;
        let glyphList = this.glyphList;
        d3.selectAll(".truthRect,.foundRect,.overlapRect,.shadowline").on("mouseover", d => {
            let currentGlyph = this.getCurrentPageGlyphs()[Math.floor(yScale.invert(event.offsetY))];
            if (currentGlyph.foundPatterns[0] !== undefined
                && currentGlyph.foundPatterns !== undefined
                && currentGlyph.foundPatterns[0].confidence !== undefined) {
                //Case: True positive or False positive --> Confidence data
                let percentile = "";
                let lowerConf = 0;
                let total = 0;
                glyphList.forEach((glyph) => {
                    total++;
                    if (glyph.foundPatterns[0] !== undefined &&
                        glyph.foundPatterns[0].confidence !== undefined
                        && glyph.foundPatterns[0].confidence < currentGlyph.foundPatterns[0].confidence) {
                        lowerConf++;
                    }
                })
                percentile = this.ordinal_suffix_of(Math.round(lowerConf / total * 100));

                if (currentGlyph.groundTruth !== null && currentGlyph.groundTruth !== undefined) {
                    d3.select("#tooltipdiv").append("p").append("strong")
                        .text("True Positive")
                        .style("color", "white");
                } else {
                    d3.select("#tooltipdiv").append("p").append("strong")
                        .text("False Positive")
                        .style("color", "white");
                }

                d3.select("#tooltipdiv").style("visibility", "visible").append("p")
                    .text("Glyph ID (in glyphlist): " + Math.floor(yScale.invert(event.offsetY)));
                d3.select("#tooltipdiv").style("visibility", "visible").append("p")
                    .text("Confidence: " + currentGlyph.foundPatterns[0].confidence);
                d3.select("#tooltipdiv").style("visibility", "visible").append("p")
                    .text("Confidence percentile: " + percentile);
                d3.select("#tooltipdiv").style("visibility", "visible").append("p")
                    .text("Glyph location in series: " +
                        "[" + currentGlyph.getSmallestStart() + "," + currentGlyph.getLargestEnd() + "]");
            } else {
                //Case: False negative --> No confidence
                d3.select("#tooltipdiv").append("p").append("strong")
                    .text("False Negative")
                    .style("color", "white");
                d3.select("#tooltipdiv").style("visibility", "visible").append("p")
                    .text("Glyph ID (in glyphlist): " + Math.floor(yScale.invert(event.offsetY)));
                d3.select("#tooltipdiv").style("visibility", "visible").append("p")
                    .text("Glyph location in series: " +
                        "[" + currentGlyph.getSmallestStart() + "," + currentGlyph.getLargestEnd() + "]");
            }

            //Add hover rectangle to give user feedback
            this.parent.append("rect")
                .attr("id", "glyphHoverRect")
                .attr("fill", "none")
                .attr("stroke", "black")
                .attr("stroke-width", 0.5)
                .attr("opacity", 0.55)
                .attr("x", 0)
                .attr("y", this.yScale(Math.floor(yScale.invert(event.offsetY))))
                .attr("width", this.xScale(currentGlyph.getLargestEnd()) - this.xScale(currentGlyph.getSmallestStart()))
                .attr("height", this.yScale(Math.floor(yScale.invert(event.offsetY))) - this.yScale(Math.floor(yScale.invert(event.offsetY)) - 0.5));

        }).on("mousemove", function () {
            let yOffset = d3.select("#tooltipdiv").node().getBoundingClientRect().height / 2;
            d3.select("#tooltipdiv").style("top", ((event.pageY) - yOffset) + "px").style("left", (event.pageX) + 15 + "px");

            // P = n/N * 100% --> Number of datapoints below data point of interest divided by total number of data points
            // Rounded to nearest INT
        }).on("mouseout", function () {
            d3.selectAll("#tooltipdiv > *").remove();
            d3.selectAll("#glyphHoverRect").remove();
            d3.select("#tooltipdiv").style("visibility", "hidden");
        });
    }

    getTimeSeriesSlice(start, end){
        return this.timeSeries.slice(start, end+1);
    }

    //TODO fix a less hacky way to do this, but this is working at least
    toggleShadowLines(){
        this.showShadowLines = !this.showShadowLines;
        if(this.showShadowLines){
            d3.selectAll(".shadowline")
                .attr("opacity", 0.22);
        } else {
            d3.selectAll(".shadowline")
                .attr("opacity", 0);
        }
    }

    setShadowLines(){
        if(this.showShadowLines){
            d3.selectAll(".shadowline")
                .attr("opacity", 0.22);
        } else {
            d3.selectAll(".shadowline")
                .attr("opacity", 0);
        }
    }

    initPagination(){
        for(let i=1; i<=this.pages; i++){
            d3.select("#paginationTarget").append("li")
                .attr("id", "paginationNumber" + i)
                .attr("class",  "pagination-link mt-0")
                .text(i)
                .on("click", d =>{
                    this.changePage(i);
                });
        }
        //Add functionality to the previous and next buttons
        d3.select("#paginationNext").on("click", d =>{
            if(!(d3.select(d.srcElement).attr("disabled") === "disabled")) {
                this.changePage(this.currentPage + 1);
            }
        })
        d3.select("#paginationPrevious").on("click", d =>{
            if(!(d3.select(d.srcElement).attr("disabled") === "disabled")){
                this.changePage(this.currentPage  - 1);
            }
        })

        //Set current page to page 1 and disable previous
        d3.select("#paginationNumber1").attr("class", "pagination-link mt-0 is-current");
        d3.select("#paginationPrevious").attr("disabled", "disabled");
    }

    changePage(newPage){
        //Deselect old current page link
        d3.select("#paginationNumber" + this.currentPage).attr("class", "pagination-link mt-0");

        this.currentPage = newPage;

        //Set new active page link
        d3.select("#paginationNumber" + this.currentPage).attr("class", "pagination-link mt-0 is-current")

        //Potentially disable next and previous buttons
        if(this.currentPage === this.pages){
            d3.select("#paginationNext").attr("disabled", "disabled");
        } else {
            d3.select("#paginationNext").attr("disabled", null);
        }

        if(this.currentPage === 1){
            d3.select("#paginationPrevious").attr("disabled", "disabled");
        } else {
            d3.select("#paginationPrevious").attr("disabled", null);
        }

        this.draw();
    }

    getCurrentPageGlyphs(){
        let glyphsList = this.glyphList;

        let start = ((this.currentPage-1) * this.glyphsperpage) + 1;
        let end = this.currentPage * this.glyphsperpage;

        let result = [];

        glyphsList.forEach((glyph, index) =>{
            //Index counts from 0 to end, we want to start at 1 for this calculation;
            let i = index+1;
            if((i>=start) && (i<=end)){
                result.push(glyph);
            }
        })
        return result;
    }

    setCurrentSelectedTrack(track){
        this.shadowLineTrack = track;
        this.sortGlyphs();

        this.shadowlineScale.domain([d3.min(this.timeSeries, d => {
            return d[this.shadowLineTrack]
        }), d3.max(this.timeSeries, d => {
            return d[this.shadowLineTrack]
        })]);

        this.draw();

    }

    initTrackSelectionSelect(){
        this.timeSeries.columns.forEach((col) =>{
            if(col !== "Time"){
                d3.select("#glyphTrackSelect").append("option")
                    .text(col);
            }
        })
        d3.select("#glyphTrackSelect").on("change", d =>{
            this.setCurrentSelectedTrack(d3.select("#glyphTrackSelect").property("value"))});
    }

    ordinal_suffix_of(i) {
        let j = i % 10,
            k = i % 100;
        if (j === 1 && k !== 11) {
            return i + "st";
        }
        if (j === 2 && k !== 12) {
            return i + "nd";
        }
        if (j === 3 && k !== 13) {
            return i + "rd";
        }
        return i + "th";
    }

    //TODO implement sliding box in time series to show pagination
}