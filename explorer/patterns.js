class Pattern{
    start;
    end;
    color;

    //If two patterns overlap, this return the start and end of overlap. Returns [0,0] if no overlap.
    getOverlap(pattern){

        let start1 = this.start;
        let end1 = this.end;
        let start2 = pattern.start;
        let end2 = pattern.end;

        if((start1 <= start2) && (start2 <= end1) || (start2 <= start1) && (start1 <= end2) ||
            ((start1<= start2) && (end1 >= end2))){
            let overlapStart = Math.max(start1, start2);
            let overlapEnd = Math.min(end1, end2);
            return [overlapStart, overlapEnd];
        } else {
            return [0,0];
        }
    }
}

//Biggest difference between FoundPattern and GroundTruth: No confidence for GroundTruth
export class FoundPattern extends Pattern{
    confidence;
    constructor(start, end, confidence, color) {
        super();

        this.start = start;
        this.end = end;
        this.confidence = confidence;
        this.color = color;
    }

    isFalsePositive(groundTruths){
        let result = true;
        groundTruths.forEach((truth)=>{
            if((this.start >= truth.start && this.start <= truth.end) ||
                (this.end >= truth.start && this.end <= truth.end) ||
                (this.start <= truth.start && this.end >= truth.end)){
                result = false;
            }
        })
        return result;
    }
}

export class GroundTruth extends Pattern{
    constructor(start, end, color) {
        super();

        this.start = start;
        this.end = end;

        this.color = color;
    }

    getMatches(foundPatterns){
        let result = [];
        //Adds all the found patterns that are within the truth to a list and returns that list
        foundPatterns.forEach((found) => {
            if((found.start >= this.start && found.start <= this.end) ||
                (found.end >= this.start && found.end <= this.end) ||
                (found.start <= this.start && found.end >= this.end)){
                result.push(found);
            }
        })

        return result;
    }
}