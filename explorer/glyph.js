export class Glyph{
    groundTruth;
    foundPatterns;

    constructor(groundtruth, matchingpatterns) {
        this.groundTruth = groundtruth;
        this.foundPatterns = matchingpatterns;
    }

    getHighestConf(){
        let highest = 0;
        this.foundPatterns.forEach((pattern) => {
            if(pattern.confidence > highest){
                highest = pattern.confidenc;
            }
        })
        return highest;
    }

    getAverageConf(){
        if(this.foundPatterns.length === 0){
            return null;
        }

        let average = 0;
        let total = 0;
        this.foundPatterns.forEach((pattern) => {
            total += pattern.confidence;
        })
        average = total/this.foundPatterns.length;
        return average;
    }

    getSmallestFoundStart(){
        let smallest = Number.MAX_VALUE;
        this.foundPatterns.forEach((pattern) =>{
            if(pattern.start < smallest){
                smallest = pattern.start;
            }
        })
        return smallest;
    }

    getSmallestStart(){
        let smallest;
        if(this.groundTruth !== null){
            smallest = this.groundTruth.start;
        } else {
            smallest = Number.MAX_VALUE;
        }

        this.foundPatterns.forEach((pattern) =>{
            if(pattern.start < smallest){
                smallest = pattern.start;
            }
        })
        return smallest;
    }

    getLargestEnd(){
        let largest;
        if(this.groundTruth !== null){
            largest = this.groundTruth.end;
        } else {
            largest = 0;
        }

        this.foundPatterns.forEach((pattern) =>{
            if(pattern.end > largest){
                largest = pattern.end;
            }
        })
        return largest;
    }

    getOverlap(){
        let result = [];
        this.foundPatterns.forEach((pattern) =>{
            if(this.groundTruth.start >= pattern.start && this.groundTruth.end <= pattern.end){
                result.push([this.groundTruth.start, this.groundTruth.end]);
            } else if(this.groundTruth.start <= pattern.start && this.groundTruth.end <= pattern.end){
                result.push([pattern.start, this.groundTruth.end]);
            } else if(this.groundTruth.start >= pattern.start && this.groundTruth.end >= pattern.end){
                result.push([this.groundTruth.start, pattern.end]);
            } else if(this.groundTruth.start <= pattern.start && this.groundTruth.end >= pattern.end){
                result.push([pattern.start, pattern.end]);
            }
        })
        return result;
    }

    getOverlapPercent(){

    }

    getGTCoveragePercent(){
        let result = [];

        if(this.foundPatterns.length > 0 && this.groundTruth !== null){
            this.foundPatterns.forEach((pattern, index) => {
                let excessLeft = this.getExcess()[index][0];
                let excessRight = this.getExcess()[index][1];
                let groundTruthLength = this.groundTruth.end - this.groundTruth.start;

                if(excessRight >= 0 && excessLeft >= 0){
                    result.push(100);
                } else if (excessLeft < 0){
                    result.push((this.groundTruth.end - pattern.start) / groundTruthLength * 100);
                } else if (excessRight < 0){
                    result.push((pattern.end - this.groundTruth.start) / groundTruthLength * 100);
                } else {
                    result.push((pattern.end - pattern.start) / groundTruthLength * 100);
                }

            })
        } else if(this.groundTruth === null) {
            result.push("False Positive")
        } else if(this.foundPatterns.length <= 0){
            result.push(0);
        }

        return result;
    }

    getExcess(){
        let result = [];

        if(this.foundPatterns.length > 0 && this.groundTruth !== null){
            this.foundPatterns.forEach((pattern) => {
                let excessLeft = this.groundTruth.start - pattern.start;
                let excessRight = pattern.end - this.groundTruth.end;

                result.push([excessLeft, excessRight]);
            })
        }
        //Result[x][0] is excess left (0 if no excess), Result[x][1] is excess right (0 if no excess)
        return result;
    }

    isExcessLeft(){
        let excess = this.getExcess();
        let excessLeft = excess[0][0];
        let excessRight = excess[0][1];

        return excessLeft >= excessRight;
    }


    getTotalExcess(){
        let result = 0;
        if(this.getExcess().length>0){
            let excess = this.getExcess()[0];

            if(excess[0] > 0){
                result += excess[0];
            }
            if(excess[1] > 0){
                result += excess[1];
            }
        }
        return result;
    }
}