import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

export class GroundTruths extends Array{

    constructor() {
        super();
    }

    getFalseNegatives(foundPatterns, confidenceCutoff){

        let falseNeg = [];

        this.forEach((truth)=>{

            let foundMatch = false;

            foundPatterns.forEach((found) =>{
                //If the start or end of the found pattern is within the truth, it is a match, so not a false negative
                if((found.start >= truth.start && found.start <= truth.end) ||
                    (found.end >= truth.start && found.end <= truth.end)){
                    if(found.confidence >= confidenceCutoff){
                        foundMatch = true;
                    }
                }
            })

            //If no match has been found, the ground truth is a false negative at this point. (flexible for cutoff)
            if(!foundMatch){
                falseNeg.push(truth);
            }
        })

        return falseNeg;
    }

    getGroundTruth(start, end){
        let result;
        this.forEach((truth) => {
            if(result.start === start && result.end === end){
                result = truth;
            }
        })
        return result;
    }
}