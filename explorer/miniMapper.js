import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

export class MiniMapper {

    parent;
    width;
    height;
    lineGrapher;

    foundPatterns;
    groundTruths;
    timeSeries;

    foundPatternColor;
    groundTruthColor;

    xScale;

    zoomRect;
    zoomLineOne;
    zoomLineTwo;

    constructor(parent, width, height, foundpatterns, groundtruths, timeseries, lineGrapher, foundPatternColor,
                groundTruthColor) {

        this.parent = parent;
        this.width = width;
        this.height = height;
        this.lineGrapher = lineGrapher;

        this.foundPatterns = foundpatterns;
        this.groundTruths = groundtruths;
        this.timeSeries = timeseries;

        this.foundPatternColor = foundPatternColor;
        this.groundTruthColor = groundTruthColor;

        //Scale translating timeseries to coordinates
        let timeMin = d3.min(this.timeSeries, d => {return d.Time});
        let timeMax = d3.max(this.timeSeries, d => {return d.Time});
        this.xScale = d3.scaleLinear()
            .domain([timeMin, timeMax])
            .range([0, this.width]);

        this.draw();
    }

    setGlyphCharter(glyphCharter){
        this.glyphCharter = glyphCharter;
    }

    draw(){
        //Draw line
        this.parent.append("line")
            .attr("x1", 0)
            .attr("x2", this.width)
            .attr("y1", this.height/2)
            .attr("y2", this.height/2)
            .attr("stroke", "black");

        //Add marks for found patterns
        this.foundPatterns.forEach((pattern) => {
            this.parent.append("line")
                .attr("x1", this.xScale(pattern.start))
                .attr("x2", this.xScale(pattern.end))
                .attr("y1", this.height/2+10)
                .attr("y2", this.height/2+10)
                .attr("stroke", this.foundPatternColor)
                .attr("stroke-width", 2.5)
                .attr("opacity", 0.6);

            let patternMiddle = (pattern.start + pattern.end) / 2

            this.parent.append("circle")
                .attr("cx", this.xScale(patternMiddle))
                .attr("cy", this.height/2 + 10)
                .attr("r", 5)
                .attr("stroke", "none")
                .attr("fill", this.foundPatternColor);
        })

        //Add marks for ground truths
        this.groundTruths.forEach((truth) => {
            this.parent.append("line")
                .attr("x1", this.xScale(truth.start))
                .attr("x2", this.xScale(truth.end))
                .attr("y1", this.height/2-10)
                .attr("y2", this.height/2-10)
                .attr("stroke", this.groundTruthColor)
                .attr("stroke-width", 2.5)
                .attr("opacity", 0.6);

            let truthMiddle = (truth.start + truth.end) / 2;

            this.parent.append("circle")
                .attr("cx", this.xScale(truthMiddle))
                .attr("cy", this.height/2 - 10)
                .attr("r", 5)
                .attr("stroke", "none")
                .attr("fill", this.groundTruthColor);
        })

        //Draw square for selection

        //DeltaX used when dragging (difference between drag point and start of square)
        let deltaX = 0;
        //Width used when dragging to make sure it doesn't go out of bounds
        let width = this.width;

        let lineGrapher = this.lineGrapher;
        let xScale = this.xScale;

        this.zoomRect = this.parent.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", this.width)
            .attr("height", this.height)
            .attr("stroke", "black")
            .attr("stroke-width", 1)
            .attr("fill", "darkgray")
            .attr("opacity", 0.5);

        let oldStart;
        let oldWidth;

        this.zoomLineOne = this.parent.append("line")
            .attr("x1", 0)
            .attr("x2", 0)
            .attr("y1", 0)
            .attr("y2", this.height)
            .attr("stroke", "blue")
            .attr("opacity", 0)
            .attr("stroke-width", 5);

        this.zoomLineTwo = this.parent.append("line")
            .attr("x1", this.width)
            .attr("x2", this.width)
            .attr("y1", 0)
            .attr("y2", this.height)
            .attr("stroke", "blue")
            .attr("opacity", 0)
            .attr("stroke-width", 5);

        let zoomLineOne = this.zoomLineOne;
        let zoomLineTwo = this.zoomLineTwo;

        this.zoomRect.on("mouseover", function(d) {
            d3.select(this).style("cursor", "move");
        }).on("mouseout", function(d){
            d3.select(this).style("cursor", "default");
        })
            .call(d3.drag()
                .on("start", function(d) {
                    //DeltaX is the difference between the start of the rect and the spot where you grabbed it
                    deltaX = d.x - d3.select(this).attr("x");
                })
                .on("drag", function(d)  {
                    //Possible, since width doesn't change while dragging
                    let dragWidth = +d3.select(this).attr("width");
                    let leftX = d.x - deltaX;
                    let rightX = d.x - deltaX + dragWidth;

                    if(leftX >= 0 && rightX <= width){
                        d3.select(this).attr("x", leftX);
                        zoomLineOne.attr("x1", leftX).attr("x2", leftX);
                        zoomLineTwo.attr("x1", rightX).attr("x2", rightX);
                    } else if (leftX < 0) {
                        d3.select(this).attr("x", 0);
                        zoomLineOne.attr("x1", 0).attr("x2", 0);
                        zoomLineTwo.attr("x1", dragWidth).attr("x2", dragWidth);
                    } else if (rightX > width){
                        d3.select(this).attr("x", width - dragWidth);
                        zoomLineOne.attr("x1", width - dragWidth).attr("x2", width - dragWidth);
                        zoomLineTwo.attr("x1", width).attr("x2", width);
                    }
                })
                .on("end", function(d){
                    let resultStart = xScale.invert(+d3.select(this).attr("x"));
                    let resultEnd = xScale.invert(+d3.select(this).attr("x") + +d3.select(this).attr("width"));

                    lineGrapher.miniMapZoom(resultStart, resultEnd);

                }));

        let zoomRect = this.zoomRect;

        this.zoomLineOne.on("mouseover", function(d) {
            d3.select(this).style("cursor", "ew-resize");
        }).on("mouseout", function(d) {
            d3.select(this).style("cursor", "default");
        })
            .call(d3.drag()
                .on("start", function(d){
                    oldStart = +zoomRect.attr("x");
                    oldWidth = +zoomRect.attr("width");
                })
                .on("drag", function(d){
                    if(d.x>0 && d.x<width){
                        d3.select(this).attr("x1", d.x).attr("x2", d.x);
                        zoomRect.attr("x", d.x).attr("width", ((oldStart + oldWidth) - d.x));
                    }
                })
                .on("end", function(d){
                    let resultStart;
                    let resultEnd;
                    if(d.x>0){
                        resultStart = xScale.invert(+d3.select(this).attr("x1"));
                        resultEnd = xScale.invert(+d3.select(this).attr("x1") + +zoomRect.attr("width"));
                    } else {
                        resultStart = xScale.invert(0);
                        resultEnd = xScale.invert(+zoomRect.attr("width"));
                    }

                    lineGrapher.miniMapZoom(resultStart, resultEnd);
                })
            );

        this.zoomLineTwo.on("mouseover", function(d) {
                d3.select(this).style("cursor", "ew-resize");
            }).on("mouseout", function(d) {
            d3.select(this).style("cursor", "default");
        })
            .call(d3.drag()
                .on("start", function(d){
                    oldStart = zoomRect.attr("x");
                })
                .on("drag", function(d){
                    if(d.x < width){
                        d3.select(this).attr("x1", d.x).attr("x2", d.x);
                        zoomRect.attr("width", d.x-oldStart);
                    }
                })
                .on("end", function(d){
                    let maxWidth = +d3.select("#miniMapTarget > svg").attr("width");
                    if(d.x < maxWidth){
                        let resultStart = xScale.invert(+d3.select(this).attr("x1") - +zoomRect.attr("width"));
                        let resultEnd= xScale.invert(+d3.select(this).attr("x1"));

                        lineGrapher.miniMapZoom(resultStart, resultEnd);
                    } else {
                        let resultStart = xScale.invert(maxWidth- +zoomRect.attr("width"));
                        let resultEnd= xScale.invert(maxWidth);

                        lineGrapher.miniMapZoom(resultStart, resultEnd);
                    }
                }));
    }

    changeLocation(start, end){
        let x1 = this.xScale(start);
        let width = this.xScale(end) - this.xScale(start);
        let x2 = x1 + width;
        this.zoomLineOne.attr("x1", x1).attr("x2", x1);
        this.zoomRect.attr("x", x1).attr("width", width);
        this.zoomLineTwo.attr("x1", x2).attr("x2", x2);
    }

    reset(){
        this.zoomLineOne.attr("x1", 0).attr("x2", 0);
        this.zoomLineTwo.attr("x1", this.width).attr("x2", this.width);
        this.zoomRect.attr("x", 0).attr("width", this.width);
    }
}