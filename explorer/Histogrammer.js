import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

export class Histogrammer{
    parent;
    height;
    width;
    foundPatterns;
    groundTruths;
    addFalsePositives;
    xScale;

    color;

    //D3 components used in selecting the cutoff
    hoverline;
    currentSelectionLine
    newCutoffText;
    currentCutoffText;

    constructor(parent, height, width, foundpatterns, groundTruths, color) {
        this.parent = parent;
        this.height = height;
        this.width = width;
        this.foundPatterns = foundpatterns;
        this.groundTruths = groundTruths;
        this.addFalsePositives = true;

        this.color = color;

        this.createConfidenceArray();

        this.draw();

        this.initHoverLine();
        this.initCutoffText();
        this.initSelectedCutoffLine();
    }

    createConfidenceArray(){
        let result = [];
        this.foundPatterns.forEach((pattern)=>{
            if(this.addFalsePositives){
                result.push(pattern.confidence);
            } else{
                if(!pattern.isFalsePositive(this.groundTruths)){
                    result.push(pattern.confidence);
                }
            }
        })
        return result;
    }

    getMostOccurrences(array){
        let occurrenceArray = new Map;
        let largestOccurrences = 0;
        array.forEach((entry) => {
            if(occurrenceArray.has(entry)){
                occurrenceArray.set(entry, occurrenceArray.get(entry)+1);
            } else {
                occurrenceArray.set(entry, 1);
            }
        })
        occurrenceArray.forEach((value, key) => {
            if (value > largestOccurrences){
                largestOccurrences = value;
            }
        })

        return largestOccurrences;
    }

    draw(){
        let confarr = this.createConfidenceArray();
        let height = this.height;
        this.xScale = d3.scaleLinear()
            .domain([0,1.01])
            .range([0, this.width]);
        let xScale = this.xScale;
        this.parent.append("g")
            .attr("transform", "translate(0," + this.height + ")")
            .call(d3.axisBottom(xScale));

        let histogram = d3.histogram()
            .value(function(d){return d})
            .domain(xScale.domain())
            .thresholds(xScale.ticks(100));

        let bins = histogram(confarr);

        let yScale = d3.scaleLinear()
            .domain([0, d3.max(bins, function(d){return d.length})])
            .range([this.height, 0]);

        let yAxisTicks = yScale.ticks()
            .filter(tick => Number.isInteger(tick));

        this.parent.append("g")
            .call(d3.axisRight(yScale).tickValues(yAxisTicks));

        this.parent.selectAll("rect")
            .data(bins)
            .enter()
            .append("rect")
            .attr("class", "histoRect")
            .attr("x", 0)
            .attr("transform", function(d) { return "translate(" + (xScale(d.x0) - 0.5 * (xScale(d.x1) - xScale(d.x0) -1)) + "," + yScale(d.length) + ")"; })
            .attr("width", function(d) { return xScale(d.x1) - xScale(d.x0) -1; })
            .attr("height", function(d) { return height - yScale(d.length); })
            .style("fill", this.color)
    }

    switchAddFalsePositives(){
        this.addFalsePositives = !this.addFalsePositives;
        this.draw();
    }


    //These functions are used to draw the line that will be used to select the cutoff. They are saved as
    // fields, to be used in the html file script for functionality
    initHoverLine(){
        this.hoverline = this.parent.append("line")
            .attr("id", "newSelectionLine")
            .attr("opacity", 0.5)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", 1)
            .attr("x1", (0))
            .attr("x2", this.xScale(0))
            .attr("y1", 0)
            .attr("y2", this.height);
    }

    initSelectedCutoffLine(){
        this.currentSelectionLine = this.parent.append("line")
            .attr("id", "currentSelectionLine")
            .attr("opacity", 1)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", 1)
            .attr("x1", this.xScale(0))
            .attr("x2", this.xScale(0))
            .attr("y1", 0)
            .attr("y2", this.height);
    }

    initCutoffText(){
        this.currentCutoffText = this.parent.append("text")
            .attr("id", "histoCurrentCutoffText")
            .attr("x", this.width/4)
            .attr("y", 0)
            .attr("font-family", "Verdana")
            .attr("font-size", 11)
            .text("Current cutoff: 0.0");

        this.newCutoffText = this.parent.append("text")
            .attr("x", this.width/4 + document.getElementById("histoCurrentCutoffText").getBBox().width + 10)
            .attr("y", 0)
            .attr("font-family", "Verdana")
            .attr("font-size", 11)
            .text("Selected: ");
    }

    lowLight(x){
        //Convert x-coordinates to a meaningful value for the visualization
        let xInvert = this.xScale.invert(x);

        //Set opacity to 0.5 if the left value of the bar is lower than the selected value
        d3.selectAll(".histoRect").attr("opacity", d =>{
            if(d.x0 > xInvert){
                return 1;
            } else {
                return 0.5;
            }
        })
    }
}