import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";
import {MiniMapper} from "/explorer/miniMapper.js";

export class LineGrapher {

    parent;
    height;
    legendHeight;
    width;
    timeseries;
    groundTruths;
    foundPatterns;

    confidenceThreshold;

    timeSeriesDrawn;

    timeSeriesColor;
    groundTruthColor;
    foundPatternColor;

    //Keeps track whether the graph has been zoomed
    zoomed = false;

    idShown;

    xScale;
    yScale;
    xAxis;
    yAxis;

    //Set the different implementations for the linegraph
    vizTechnique = "BoundedBox";

    //Keep track of the selected tracks in the select box
    selectedColumns;

    miniMapper;

    constructor(parent, height, width, timeseries, groundTruths, foundPatterns, timeSeriesColor, groundTruthColor,
                foundPatternColor, miniMapParent) {
        this.parent = parent;
        this.height = height/15*14;
        this.legendHeight = height/4;
        this.width = width;
        this.timeseries = timeseries;
        this.groundTruths = groundTruths;
        this.foundPatterns = foundPatterns;
        this.timeSeriesDrawn = false;

        this.selectedColumns = timeseries.columns[1];

        this.confidenceThreshold = 0.1;

        this.timeSeriesColor = timeSeriesColor;
        this.groundTruthColor = groundTruthColor;
        this.foundPatternColor = foundPatternColor;

        //Generate scales
        this.xScale = d3.scaleLinear()
            .domain([0, this.timeseries.length])
            .range([0, this.width]);

        let yScaleDomainMin = Number.MAX_VALUE;
        let yScaleDomainMax = 0;

        this.timeseries.forEach((row)=>{
            let keys = Object.keys(row);
            keys.forEach((key) => {
                if(key !== "Time"){
                    if(row[key] > yScaleDomainMax){
                        yScaleDomainMax = row[key];
                    }
                    if(row[key] < yScaleDomainMin){
                        yScaleDomainMin = row[key];
                    }
                }
            })
        })


        this.yScale = d3.scaleLinear()
            .domain([yScaleDomainMin, yScaleDomainMax])
            .range([this.height, 0]);

        //Values and scale for converting smaller timesteps to indices
        let timeMin = d3.min(this.timeseries, d => {return d.Time});
        let timeMax = d3.max(this.timeseries, d => {return d.Time});
        let seriesLength = this.timeseries.length;
        this.timeSeriesScale = d3.scaleLinear()
            .domain([timeMin, timeMax])
            .range([0, seriesLength]);

        this.miniMapper = new MiniMapper(miniMapParent,this.width,75,this.foundPatterns,this.groundTruths,
            this.timeseries, this, this.foundPatternColor, this.groundTruthColor);

        //Generate selector for tracks
        this.generateTrackSelector();

        //Draw graph
        this.draw();
    }

    draw(){

        //update the yscale with only the selected tracks' max and min
        this.updateYScale();

        d3.selectAll("#lineGraphTarget > svg > g > *").remove();

        //Draw legend always after emptying the svg.
        this.drawLegend();

        //Add clippath, removing everything drawn out of range for zooming purposes
        this.parent.append("defs").append("svg:clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("width", this.width)
            .attr("height", this.height/3*4)
            .attr("x", 0)
            .attr("y", 0);


        let xScale = this.xScale;
        let yScale = this.yScale;

        //Loop over all tracks of the timeseries
        this.timeseries.columns.forEach((col) => {
            if(this.selectedColumns.includes(col)) {
                this.parent.append("path")
                    .datum(this.timeseries)
                    .attr("class", "timeSeriesLine")
                    .attr("fill", "none")
                    .attr("stroke", this.timeSeriesColor)
                    .attr("stroke-width", 1)
                    .attr("d", d3.line()
                        .x((d, i) => {
                            return xScale(i);
                        })
                        .y(d => {
                            return yScale(d[col]);
                        }))
                    .attr("shape-rendering", "auto")
                    .on("mouseover", function() {
                        let tooltip = d3.select("#lineGraphTooltipDiv");
                        tooltip.style("visibility", "visible").append("p")
                            .text("Track: " + col);
                    })
                    .on("mousemove", function(){
                        let tooltip = d3.select("#lineGraphTooltipDiv");
                        let yOffset = d3.select("#lineGraphTooltipDiv").node().getBoundingClientRect().height / 2;
                        tooltip.style("top", ((event.pageY) - yOffset) + "px").style("left", (event.pageX) + 15 + "px");
                    })
                    .on("mouseout", function(){
                        d3.selectAll("#lineGraphTooltipDiv > *").remove();
                        d3.select("#lineGraphTooltipDiv").style("visibility", "hidden")
                    });
            }
        })

        this.timeSeriesDrawn = true;


        this.timeseries.columns.forEach((col) => {
            if(this.selectedColumns.includes(col)){
                this.groundTruths.forEach((truth)=>{
                    let truthData = this.getTimeSeriesSlice(this.timeSeriesScale(truth.start), this.timeSeriesScale(truth.end));

                    if(this.vizTechnique === "DashedLines"){
                        //This implementation uses dashed lines for the found patterns to show overlap

                        this.parent.append("path")
                            .datum(truthData)
                            .attr("class", "groundTruthLine")
                            .attr("fill", "none")
                            .attr("stroke", this.groundTruthColor)
                            .attr("stroke-width", 1)
                            .attr("d", d3.line()
                                .x((d,i)=>{
                                    if(!this.zoomed){
                                        return xScale(i) + xScale(this.timeSeriesScale(truth.start));
                                    } else {
                                        return xScale(i) + xScale.domain()[0] + xScale(this.timeSeriesScale(truth.start));
                                    }
                                })
                                .y(d=>{
                                    return yScale(d[col]) ;
                                }))
                            .attr("shape-rendering", "auto");
                    } else if (this.vizTechnique === "BoundedBox"){
                        //This implementation changes the found pattern to a box, bounded by dashed lines
                        this.parent.append("path")
                            .datum(truthData)
                            .attr("class", "groundTruthLine")
                            .attr("fill", "none")
                            .attr("stroke", this.groundTruthColor)
                            .attr("stroke-width", 1)
                            .attr("d", d3.line()
                                .x((d,i)=>{
                                    if(!this.zoomed){
                                        return xScale(i) + xScale(this.timeSeriesScale(truth.start));
                                    } else {
                                        return xScale(i+ xScale.domain()[0]) + xScale(this.timeSeriesScale(truth.start));
                                    }
                                })
                                .y(d=>{
                                    return yScale(d[col]);
                                }))
                            .attr("shape-rendering", "auto");

                    } else if (this.vizTechnique === "OpacityDelta"){
                        //This implementation has different opacities to show overlap

                        this.parent.append("path")
                            .datum(truthData)
                            .attr("class", "groundTruthLine")
                            .attr("fill", "none")
                            .attr("stroke", this.groundTruthColor)
                            .attr("stroke-width", 1)
                            .attr("d", d3.line()
                                .x((d,i)=>{
                                    if(!this.zoomed){
                                        return xScale(i) + xScale(this.timeSeriesScale(truth.start));
                                    } else {
                                        return xScale(i + xScale.domain()[0]) + xScale(this.timeSeriesScale(truth.start));
                                    }
                                })
                                .y(d=>{
                                    return yScale(d[col]) ;
                                }))
                            .attr("shape-rendering", "auto");
                    }
                })
            }
        })

        if (this.vizTechnique === "BoundedBox") {
            this.foundPatterns.forEach((pattern) => {
                if(pattern.confidence >= this.confidenceThreshold){
                    let patternData = this.getTimeSeriesSlice(this.timeSeriesScale(pattern.start), this.timeSeriesScale(pattern.end));
                    //This implementation changes the found pattern to a box, bounded by dashed lines
                    this.parent.append("rect")
                        .attr("class", "foundpatternRect")
                        .attr("fill", this.foundPatternColor)
                        .attr("opacity", 0.3)
                        .attr("x", xScale(this.timeSeriesScale(pattern.start)))
                        .attr("y", 0)
                        .attr("height", this.height)
                        .attr("width", xScale(this.timeSeriesScale(pattern.end)) - xScale(this.timeSeriesScale(pattern.start)));

                    this.parent.append("line")
                        .attr("class", "dashedLine")
                        .style("stroke-dasharray", ("3,3"))
                        .attr("fill", "none")
                        .attr("stroke", this.foundPatternColor)
                        .attr("stroke-width", 1)
                        .attr("x1", xScale(this.timeSeriesScale(pattern.start)))
                        .attr("x2", xScale(this.timeSeriesScale(pattern.start)))
                        .attr("y1", 0)
                        .attr("y2", this.height);

                    this.parent.append("line")
                        .attr("class", "dashedLine")
                        .style("stroke-dasharray", ("3,3"))
                        .attr("fill", "none")
                        .attr("stroke", this.foundPatternColor)
                        .attr("stroke-width", 1)
                        .attr("x1", xScale(this.timeSeriesScale(pattern.end)))
                        .attr("x2", xScale(this.timeSeriesScale(pattern.end)))
                        .attr("y1", 0)
                        .attr("y2", this.height);
                }
            })
        } else {
            //Draw patterns for all columns in the data
            this.timeseries.columns.forEach((col) =>{
                if(this.selectedColumns.includes(col)){
                    this.foundPatterns.forEach((pattern)=>{
                        if(pattern.confidence >= this.confidenceThreshold){
                            let patternData = this.getTimeSeriesSlice(this.timeSeriesScale(pattern.start), this.timeSeriesScale(pattern.end));
                            if(this.vizTechnique === "DashedLines"){
                                //This implementation uses dashed lines for the found patterns to show overlap

                                this.parent.append("path")
                                    .datum(patternData)
                                    .attr("class", "groundTruthLine")
                                    .attr("fill", "none")
                                    .attr("stroke", this.foundPatternColor)
                                    .attr("stroke-width", 1)
                                    .attr("stroke-dasharray", ("6, 6"))
                                    .attr("d", d3.line()
                                        .x((d,i)=>{
                                            if(!this.zoomed){
                                                return xScale(i) + xScale(this.timeSeriesScale(pattern.start));
                                            } else {
                                                return xScale(i + xScale.domain()[0]) + xScale(this.timeSeriesScale(pattern.start));
                                            }
                                        })
                                        .y(d=>{
                                            return yScale(d[col]);
                                        }))
                                    .attr("shape-rendering", "auto");
                            } else if(this.vizTechnique === "OpacityDelta") {
                                //This implementation has different opacities to show overlap

                                this.parent.append("path")
                                    .datum(patternData)
                                    .attr("class", "groundTruthLine")
                                    .attr("fill", "none")
                                    .attr("stroke", this.foundPatternColor)
                                    .attr("opacity", 0.5)
                                    .attr("stroke-width", 1)
                                    .attr("d", d3.line()
                                        .x((d,i)=>{
                                            if(!this.zoomed){
                                                return xScale(i) + xScale(this.timeSeriesScale(pattern.start));
                                            } else {
                                                return xScale(i + xScale.domain()[0]) + xScale(this.timeSeriesScale(pattern.start));
                                            }
                                        })
                                        .y(d=>{
                                            return yScale(d[col]);
                                        }))
                                    .attr("shape-rendering", "auto");
                            }
                        }
                    })
                }
            })
        }

        //Add the clip path to everything in the SVG
        this.parent.selectAll("g > *")
            .attr("clip-path", "url(#clip)");

        //Generate Axis over data
        if(!this.zoomed){
            this.xScale.domain([0, this.timeSeriesScale.invert(this.timeseries.length)]);
            this.xAxis = (this.parent.append("g")
                .attr("transform", "translate(0," + this.height + ")"))
                .call(d3.axisBottom(this.xScale));
            this.xScale.domain([0, this.timeseries.length]);
        } else {
            let domainMin = this.xScale.domain()[0];
            let domainMax = this.xScale.domain()[1];
            this.xScale.domain([this.timeSeriesScale.invert(domainMin), this.timeSeriesScale.invert(domainMax)]);
            this.xAxis = (this.parent.append("g")
                .attr("transform", "translate(0," + this.height + ")"))
                .call(d3.axisBottom(this.xScale));
            this.xScale.domain(domainMin, domainMax);
        }
        this.yAxis = this.parent.append("g")
            .call(d3.axisRight(this.yScale));
    }

    getTimeSeriesSlice(start, end){
        return this.timeseries.slice(start, end+1);
    }

    setNewVizTechnique(newTech){
        this.vizTechnique = newTech;
        this.draw();
    }

    setSelectedColumns(newSelection){
        this.selectedColumns = newSelection;
        this.draw();
    }

    generateTrackSelector(){
        d3.select("#trackSelector").attr("multiple", "multiple")
            .attr("size", Math.min(this.timeseries.columns.length - 1, 8));

        this.timeseries.columns.forEach((col) => {
            if(col !== "Time"){
                if(this.selectedColumns.includes(col)){
                    d3.select("#trackSelector").append("option")
                        .attr("value", col)
                        .attr("selected", "selected")
                        .text(col);
                } else {
                    d3.select("#trackSelector").append("option")
                        .attr("value", col)
                        .text(col);
                }
            }
        })
    }

    updateXScale(start, end){
        if(this.zoomed){
            //Case zooming out
            this.xScale.domain([this.timeSeriesScale(start), this.timeSeriesScale(end)]);
            this.toggleZoomed();
            this.draw();
        } else {
            //Case zooming in
            this.xScale.domain([this.timeSeriesScale(start) - 50, this.timeSeriesScale(end) + 50]);
            this.toggleZoomed();
            this.draw();
        }
    }

    miniMapZoom(start, end){
        if(!this.zoomed){
            this.zoomed = !this.zoomed;
        }
        this.xScale.domain([this.timeSeriesScale(start), this.timeSeriesScale(end)]);
        this.draw();
    }

    resetZoom(){
        if(this.zoomed){
            this.zoomed = !this.zoomed;
        }
        let timeMin = d3.min(this.timeseries, d => {
            return d.Time;
        });
        let timeMax = d3.max(this.timeseries, d => {
            return d.Time;
        });
        this.xScale.domain([this.timeSeriesScale(timeMin), this.timeSeriesScale(timeMax)]);
        this.draw();
    }

    toggleZoomed(){
        this.zoomed = !this.zoomed;
    }

    updateYScale(){
        let largest = 0;
        let smallest = Number.MAX_VALUE;

        this.timeseries.forEach((row) => {
            let keys = Object.keys(row);
            keys.forEach((key) => {
                if(key !== "Time" && this.selectedColumns.includes(key)){
                    if(row[key] > largest){
                        largest = row[key];
                    }
                    if(row[key] < smallest){
                        smallest = row[key];
                    }
                }
            })
        })

        this.yScale.domain([smallest, largest]);
        if(this.yAxis !== undefined){
            this.yAxis.transition().call(d3.axisRight(this.yScale));
        }
    }

    drawLegend(){
        this.parent.append("line")
            .attr("x1", 0)
            .attr("x2", 30)
            .attr("y1", this.height + 30)
            .attr("y2", this.height + 30)
            .attr("stroke", this.timeSeriesColor)
            .attr("stroke-width", 2.5);

        this.parent.append("text")
            .attr("x", 35)
            .attr("y", this.height + 34)
            .text("Time Series Line");

        this.parent.append("line")
            .attr("x1", 300)
            .attr("x2", 330)
            .attr("y1", this.height + 30)
            .attr("y2", this.height + 30)
            .attr("stroke", this.groundTruthColor)
            .attr("stroke-width", 2.5);

        this.parent.append("text")
            .attr("x", 335)
            .attr("y", this.height+34)
            .text("Ground Truth Line");

        this.parent.append("rect")
            .attr("x", 600)
            .attr("y", this.height+22)
            .attr("width", 30)
            .attr("height", 15)
            .attr("fill", this.foundPatternColor)
            .attr("fill-opacity", 0.3)
            .attr("stroke", this.foundPatternColor)
            .attr("stroke-opacity", 1)
            .style("stroke-dasharray", ("3,3"));

        this.parent.append("text")
            .attr("x", 635)
            .attr("y", this.height+34)
            .text("Found Pattern Box");
    }

    drawIds(glyphlist){
        d3.selectAll(".lineGraphIDText").remove();
        d3.selectAll(".lineGraphIDCircle").remove();

        if(!this.zoomed) {

            //Padding used to leave some room between numbers, when numbers are in the same location verticalIndent moves
            //the second one down (alternating or adding)
            let idPadding = 5;
            let verticalIndent = 0;
            let lastX = 0;

            glyphlist.forEach((glyph, index) => {

                let x = this.timeSeriesScale(glyph.getSmallestStart());

                if (((lastX - idPadding < x) && (x < lastX + idPadding))) {
                    //Indent the id to below the last one
                    verticalIndent++;

                    this.parent.append("circle")
                        .attr("class", "lineGraphIDCircle")
                        .attr("cx", this.xScale(this.timeSeriesScale(glyph.getSmallestStart())) + 5)
                        .attr("cy", verticalIndent * 20 + 15)
                        .attr("r", 10)
                        .attr("fill", "none")
                        .attr("stroke", "black")
                        .attr("shape-rendering", "geometricPrecision");

                    this.parent.append("text")
                        .attr("class", "lineGraphIDText")
                        .attr("font-family", "roboto")
                        .attr("font-size", "14")
                        .attr("x", d => {
                            if (glyphlist.indexOf(glyph).toString().length === 1) {
                                return this.xScale(this.timeSeriesScale(glyph.getSmallestStart()));
                            } else if (glyphlist.indexOf(glyph).toString().length === 2) {
                                return this.xScale(this.timeSeriesScale(glyph.getSmallestStart())) - 5;
                            } else {
                                return this.xScale(this.timeSeriesScale(glyph.getSmallestStart())) - 10;
                            }
                        })
                        .attr("y", verticalIndent * 20 + 20)
                        .text(index);
                } else {
                    this.parent.append("circle")
                        .attr("class", "lineGraphIDCircle")
                        .attr("cx", this.xScale(this.timeSeriesScale(glyph.getSmallestStart())) + 5)
                        .attr("cy", 15)
                        .attr("r", 10)
                        .attr("fill", "none")
                        .attr("stroke", "black")
                        .attr("shape-rendering", "geometricPrecision");

                    this.parent.append("text")
                        .attr("class", "lineGraphIDText")
                        .attr("font-family", "roboto")
                        .attr("font-size", "14")
                        .attr("x", d => {
                            if (glyphlist.indexOf(glyph).toString().length === 1) {
                                return this.xScale(this.timeSeriesScale(glyph.getSmallestStart()));
                            } else if (glyphlist.indexOf(glyph).toString().length === 2) {
                                return this.xScale(this.timeSeriesScale(glyph.getSmallestStart())) - 5;
                            } else {
                                return this.xScale(this.timeSeriesScale(glyph.getSmallestStart())) - 10;
                            }
                        })
                        .attr("y", 20)
                        .text(index);
                    verticalIndent = 0;
                }

                lastX = x;
            })
        }
    }

    getTextWidth(text, fontSize, fontFace) {
        let canvas = document.createElement('canvas'),
            context = canvas.getContext('2d');
        context.font = fontSize + "px" + fontFace;
        return context.measureText(text).width;
    }

    updateConfidenceThreshold(value){
        this.confidenceThreshold = value;
        this.draw();
    }
}