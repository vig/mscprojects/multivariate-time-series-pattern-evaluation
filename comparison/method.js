export class Method {

    name;
    patternList;
    color;

    constructor(name, patterns, color) {

        this.name = name;
        this.patternList = patterns;
        this.color = color;

    }

    getRelevantPatterns(cutoff){
        let result = [];
        this.patternList.forEach((pattern) => {
            if(pattern.confidence >= cutoff){
                result.push(pattern);
            }
        })
        return result;
    }

    getConfidences(){
        let result = [];
        this.patternList.forEach((pattern) => {
            result.push(pattern.confidence);
        })
        return result;
    }
}