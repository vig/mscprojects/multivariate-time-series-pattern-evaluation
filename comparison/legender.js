import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

export class Legender {

    methodList;
    parent;

    width;
    height;

    constructor(parent, methodList, width, height) {
        this.methodList = methodList;
        this.parent = parent;

        this.width = width;
        this.height = height;

        this.draw();
    }

    draw(){
        //First show what a ground truth looks like in the glyph chart
        this.parent.append("line")
            .attr("x1", 10)
            .attr("x2", 10)
            .attr("y1", 0)
            .attr("y2", this.height)
            .attr("stroke", "grey")
            .attr("stroke-dasharray", 6);

        this.parent.append("rect")
            .attr("x", 10)
            .attr("y", 0)
            .attr("width", 20)
            .attr("height", this.height)
            .attr("fill", "lightgrey")
            .attr("stroke", "none");

        this.parent.append("line")
            .attr("x1", 30)
            .attr("x2", 30)
            .attr("y1", 0)
            .attr("y2", this.height)
            .attr("stroke", "grey")
            .attr("stroke-dasharray", 6);

        this.parent.append("text")
            .attr("x", 31)
            .attr("y", (this.height/2))
            .text("Ground Truth");


        this.methodList.forEach((method, index) => {
            this.parent.append("rect")
                .attr("x", (index + 1) * 120)
                .attr("y", 0)
                .attr("width", 20)
                .attr("height", this.height)
                .attr("stroke", "none")
                .attr("fill", method.color);

            this.parent.append("text")
                .attr("x", ((index + 1) * 120) + 20)
                .attr("y", this.height/2)
                .text(method.name);
        })
    }
}