export class TruthMatching {

    truth;
    methodList;
    methodMatchMap;

    constructor(truth, methodList, id ){
        this.truth = truth;
        this.methodList = methodList;

        //ID set on initial creation, which keeps track of the chronological id of the truth.
        this.id = id;

        if(truth !== null){
            this.methodMatchMap = this.generateMatchMap();
        }
    }

    generateMatchMap(){
        let result = new Map();
        this.methodList.forEach((method) => {
            let methodMatches = [];
            method.patternList.forEach((pattern) => {
                if(this.truth.isMatch(pattern)){
                    methodMatches.push(pattern);
                }
            })
            result.set(method, methodMatches);
        })
        return result;
    }

    getLargestSize(){
        return this.getLargestEnd() - this.getSmallestStart();
    }

    getSmallestStart(){
        let smallestStart = this.truth.start;
        this.methodMatchMap.keys().forEach((method) => {
            let matchList = this.methodMatchMap.get(method);
            matchList.forEach((pattern) => {
                if(pattern.start < smallestStart) smallestStart = pattern.start;
            })
        })
        return smallestStart;
    }

    getLargestEnd(){
        let largestEnd = this.truth.end;
        this.methodMatchMap.keys().forEach((method) => {
            let matchList = this.methodMatchMap.get(method);
            matchList.forEach((pattern) => {
                if(pattern.end > largestEnd) largestEnd = pattern.end;
            })
        })
        return largestEnd;
    }

    getAverageConfidence(){
        let totalConf = 0;
        let patternNo = 0;
        this.methodMatchMap.forEach((matches) => {
            matches.forEach((match) => {
                totalConf += match.confidence;
                patternNo ++;
            })
        })

        return totalConf/patternNo;
    }

    getHighestConfidence(){
        let highest = 0;
        this.methodMatchMap.forEach((matches) => {
            matches.forEach((match) => {
                highest = Math.max(highest, match.confidence);
            })
        })
        return highest;
    }

    getLowestConfidence(){
        let lowest = Number.maxValue;
        this.methodMatchMap.forEach((matches) => {
            matches.forEach((match) => {
                lowest = Math.min(lowest, match.confidence);
            })
        })
        return lowest;
    }

    getDiscrepancy(){
        let discrepancy = 0;
        let lowest = Number.maxValue;
        let highest = 0;
        this.methodMatchMap.forEach((matches) => {
            matches.forEach((match) => {
                lowest = Math.min(lowest, match.confidence);
                highest = Math.max(highest, match.confidence);
            })
        })
        discrepancy = highest - lowest;
        return discrepancy;
    }

    getMethodAmount(){
        return this.methodMatchMap.size;
    }

}