import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";
import {TruthMatching} from "./truthMatching.js";

export class LineGrapher{

    parent;
    width;
    height;

    groundTruthList;
    methodList;
    timeSeries;
    truthMatchArr;

    activeMethods;

    timeSeriesColor;

    xScale;
    yScale;
    timeScale;
    bottomYScale;

    constructor(parent, width, height, groundTruthList, methodList, timeSeries, timeSeriesColor, groundTruthColor) {
        this.parent = parent;
        this.width = width;
        this.height = height;
        this.graphHeight = this.height/5*4;
        this.groundTruthList = groundTruthList;
        this.methodList = methodList;
        this.timeSeries = timeSeries;
        this.activeMethods = [];

        this.methodList.forEach((method) => {
            this.activeMethods.push(method);
        })

        this.timeSeriesColor = timeSeriesColor;

        this.xScale = d3.scaleLinear()
            .domain([0, this.timeSeries.length])
            .range([0, this.width]);


        let timeMin = d3.min(this.timeSeries, d => {return +d.time});
        let timeMax = d3.max(this.timeSeries, d => {return +d.time});
        this.timeScale = d3.scaleLinear()
            .domain([timeMin, timeMax])
            .range([0,this.timeSeries.length]);

        this.yScale = d3.scaleLinear()
            .domain([d3.min(this.timeSeries, d => {
                return +d.current;
            }), d3.max(this.timeSeries, d => {
                return +d.current;
            })])
            .range([this.graphHeight, 0]);

        this.bottomYScale = d3.scaleLinear()
            .domain([0,this.activeMethods.length])
            .range([this.height/5*4 + 50, this.height]);

        this.draw();
    }

    draw(){
        d3.selectAll("#lineGraphTarget > svg > *").remove();

        //Add clippath, removing everything drawn out of range for zooming purposes
        this.parent.append("defs").append("svg:clipPath")
            .attr("id", "clip2")
            .append("rect")
            .attr("width", this.width)
            .attr("height", this.height)
            .attr("x", 0)
            .attr("y", 0);


        this.groundTruthList.forEach((truth) => {

            this.parent.append("rect")
                .attr("x", this.xScale(this.timeScale(truth.start)))
                .attr("y", 0)
                .attr("height", this.height)
                .attr("width", this.xScale(this.timeScale(truth.end)) - this.xScale(this.timeScale(truth.start)))
                .attr("opacity", 0.8)
                .attr("fill", "lightgrey");

            this.parent.append("line")
                .attr("x1", this.xScale(this.timeScale(truth.start)))
                .attr("x2", this.xScale(this.timeScale(truth.start)))
                .attr("y1", 0)
                .attr("y2", this.height)
                .attr("stroke", "grey")
                .attr("stroke-dasharray", 6);

            this.parent.append("line")
                .attr("x1", this.xScale(this.timeScale(truth.end)))
                .attr("x2", this.xScale(this.timeScale(truth.end)))
                .attr("y1", 0)
                .attr("y2", this.height)
                .attr("stroke", "grey")
                .attr("stroke-dasharray", 6);

        })

        //Add clippath, removing everything drawn out of range for zooming purposes
        this.parent.append("defs").append("svg:clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("width", this.width)
            .attr("height", this.height)
            .attr("x", 0)
            .attr("y", 0);

        this.parent.append("g").call(d3.axisRight(this.yScale));
        this.parent.append("g").attr("transform","translate(0," + this.graphHeight + ")").call(d3.axisBottom(this.xScale));


        this.parent.append("path")
            .datum(this.timeSeries)
            .attr("fill", "none")
            .attr("stroke", this.timeSeriesColor)
            .attr("stroke-width", 1)
            .attr("d", d3.line()
            .x((d, i) => {
                return this.xScale(i);
            })
            .y(d => {
                return this.yScale(+d.current);
            }))
            .attr("shape-rendering", "auto");



        //Here we set domain to the amount of active methods to create an index scale.
        this.bottomYScale
            .domain([0,this.activeMethods.length]);
        this.activeMethods.forEach((method, index) => {
            method.patternList.forEach((pattern) => {
                this.parent.append("line")
                    .attr("x1", this.xScale(this.timeScale(pattern.start)))
                    .attr("x2", this.xScale(this.timeScale(pattern.end)))
                    .attr("y1", this.bottomYScale(index))
                    .attr("y2", this.bottomYScale(index))
                    .attr("stroke-width", 10)
                    .attr("stroke", method.color);
            })
        })


        //TODO fix clippath bug with axis labels
        this.parent.selectAll("*")
            .attr("clip-path", "url(#clip2)");
    }

    toggleMethod(name){
        let found = false;
        this.activeMethods.forEach((method) => {
            if(method.name === name){
                found = true;
                this.activeMethods.remove(method);
            }
        })
        if(!found){
            this.methodList.forEach((method) => {
                if(method.name === name){
                    this.activeMethods.push(method);
                }
            })
        }
    }

    getTimeSeriesSlice(start, end){
        return this.timeseries.slice(start, end+1);
    }

    zoom(start, end){
        this.xScale.domain([this.timeScale(start), this.timeScale(end)]);
        this.draw();
    }

    zoomByPattern(pattern){
        this.zoom(pattern.start, pattern.end);
    }
}