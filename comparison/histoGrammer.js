import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";
import {TruthMatching} from "./truthMatching.js";

export class HistoGrammer{

    parent;
    height;
    width;

    groundTruths;
    methodList;
    truthMatchArr;

    selectedMethod;

    currentCutoff = 0;

    xScale;

    constructor(parent, height, width, methodlist, groundtruths) {
        this.parent = parent;
        this.height = height;
        this.width = width;
        this.groundTruths = groundtruths;
        this.methodList = methodlist;

        this.truthMatchArr = [];
        this.groundTruths.forEach((truth, index) => {
            this.truthMatchArr.push(new TruthMatching(truth, methodlist, index));
        })

        this.selectedMethod = this.truthMatchArr[0].methodList[0].name;

        this.initMethodSelector();

        this.draw();
    }

    createConfidenceArray(){
        let result = [];
        this.methodList.forEach((method) => {
            if(method.name === this.selectedMethod) {
                method.patternList.forEach((pattern) => {
                    result.push(pattern.confidence);
                })
            }
        })
        return result;
    }

    updateSelectedMethod(newMethod){
        this.selectedMethod = newMethod;
        this.draw();
    }

    initMethodSelector(){
        this.methodList.forEach((method) => {
            d3.select("#histoMethodSelect")
                .append("option")
                .attr("name", method.name)
                .attr("value", method.name)
                .attr("selected", d => {
                    if (method.name === this.selectedMethod){
                        return "selected";
                    } else {
                        return null;
                    }
                })
                .text(method.name);
        })

        d3.select("#histoMethodSelect").on("change", d => {
            this.updateSelectedMethod(d3.select("#histoMethodSelect").property("value"));
        })
    }

    draw(){
        d3.selectAll("#histoTarget > svg > g > *").remove();

        let confArr = this.createConfidenceArray();

        this.xScale = d3.scaleLinear()
            .domain([0,1])
            .range([0,this.width]);

        this.parent.append("g")
            .attr("transform", "translate(0," + this.height + ")")
            .call(d3.axisBottom(this.xScale));

        this.parent.append("text")
            .attr("text-anchor", "end")
            .attr("x", this.width)
            .attr("y", this.height + 30)
            .attr("font-family", "Libre Franklin")
            .attr("font-size", 11)
            .text("Confidence");

        this.initHoverLine();
        this.initCutoffText();
        this.initSelectedCutoffLine();

        let histogram = d3.histogram()
            .value(function(d){return d})
            .domain(this.xScale.domain())
            .thresholds(this.xScale.ticks(100));

        let bins = histogram(confArr);

        let yScale = d3.scaleLinear()
            .domain([0, d3.max(bins, function(d){return d.length})])
            .range([this.height, 0]);

        this.parent.append("g")
            .call(d3.axisRight(yScale));

        this.parent.append("text")
            .attr("text-anchor", "end")
            .attr("y", -15)
            .attr("dy", ".7em")
            .attr("font-family", "Libre Franklin")
            .attr("font-size", 11)
            .attr("transform", "rotate(-90)")
            .text("Occurrences");

        this.parent.selectAll("rect")
            .data(bins)
            .enter()
            .append("rect")
            .attr("class", "histoRect")
            .attr("x", 1)
            .attr("transform", d => { return "translate(" + this.xScale(d.x0) + "," + yScale(d.length) + ")"; })
            .attr("width", d => { return this.xScale(d.x1) - this.xScale(d.x0) -1; })
            .attr("height", d => { return this.height - yScale(d.length); })
            .style("fill", "#FFE6AB");
    }

    initHoverLine(){
        this.hoverline = this.parent.append("line")
            .attr("id", "newSelectionLine")
            .attr("opacity", 0.5)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", 1)
            .attr("x1", this.xScale(0))
            .attr("x2", this.xScale(0))
            .attr("y1", 0)
            .attr("y2", this.height);
    }

    initSelectedCutoffLine(){
        this.currentSelectionLine = this.parent.append("line")
            .attr("id", "currentSelectionLine")
            .attr("opacity", 1)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", 1)
            .attr("x1", this.xScale(this.currentCutoff))
            .attr("x2", this.xScale(this.currentCutoff))
            .attr("y1", 0)
            .attr("y2", this.height);
    }

    initCutoffText(){
        this.currentCutoffText = this.parent.append("text")
            .attr("id", "histoCurrentCutoffText")
            .attr("x", this.width/10)
            .attr("y", 0)
            .attr("font-family", "Libre Franklin")
            .attr("font-size", 11)
            .text("Current cutoff: 0.0");

        this.newCutoffText = this.parent.append("text")
            .attr("x", this.width/10 + document.getElementById("histoCurrentCutoffText").getBBox().width + 15)
            .attr("y", 0)
            .attr("font-family", "Libre Franklin")
            .attr("font-size", 11)
            .text("Selected: ");
    }

    lowLight(x){
        //Convert x-coordinates to a meaningful value for the visualization
        let xInvert = this.xScale.invert(x);

        //Set opacity to 0.5 if the left value of the bar is lower than the selected value
        d3.selectAll(".histoRect").attr("opacity", d =>{
            if(d.x0 > xInvert){
                return 1;
            } else {
                return 0.5;
            }
        })
    }
}