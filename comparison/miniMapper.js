import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

export class MiniMapper {

    parent
    width;
    height;
    lineGrapher;

    timeSeries;
    groundTruths;

    xScale;

    zoomRect;
    zoomLineOne;
    zoomLineTwo;

    constructor(parent, width, height, timeseries, groundTruths, lineGrapher){

        this.parent = parent;
        this.width = width;
        this.height = height;
        this.lineGrapher = lineGrapher;

        this.timeSeries = timeseries;
        this.groundTruths = groundTruths;

        //Scale translating timeseries to coordinates
        let timeMin = d3.min(this.timeSeries, d => {return +d.time});
        let timeMax = d3.max(this.timeSeries, d => {return +d.time});
        this.xScale = d3.scaleLinear()
            .domain([timeMin, timeMax])
            .range([0, this.width]);

        this.draw();

    }

    draw(){

        //Add clippath, removing everything drawn out of range for zooming purposes
        this.parent.append("defs").append("svg:clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("width", this.width)
            .attr("height", this.height)
            .attr("x", 0)
            .attr("y", 0);

        //Draw line
        this.parent.append("line")
            .attr("x1", 0)
            .attr("x2", this.width)
            .attr("y1", this.height/2)
            .attr("y2", this.height/2)
            .attr("stroke", "black");

        this.parent.append("line")
            .attr("x1", 1)
            .attr("x2", 1)
            .attr("y1", this.height/4)
            .attr("y2", this.height/4*3)
            .attr("stroke", "black");

        this.parent.append("line")
            .attr("x1", this.width-1)
            .attr("x2", this.width-1)
            .attr("y1", this.height/4)
            .attr("y2", this.height/4*3)
            .attr("stroke", "black");

        this.groundTruths.forEach((truth) => {
            this.parent.append("line")
                .attr("x1", this.xScale(truth.start))
                .attr("x2", this.xScale(truth.end))
                .attr("y1", this.height/2)
                .attr("y2", this.height/2)
                .attr("stroke", "black")
                .attr("stroke-width", "4")
        })

        let mousedown1 = false;
        this.parent.append("circle")
            .attr("id", "timeSelectCircle1")
            .attr("cx", 0)
            .attr("cy", this.height/2)
            .attr("r", 10)
            .attr("stroke", "darkgray")
            .attr("fill", "darkgray")
            .attr("fill-opacity", 0.5)
            .on("mousedown", d => {
                mousedown1 = true;
                console.log(mousedown1);
            })
            .on("mousemove", function(d) {
                if(mousedown1){
                    d3.select(this).attr("cx", d.offsetX);
                }
            })
            .on("mouseout", function(d) {
                if(mousedown1){
                    d3.select(this).attr("cx", d.offsetX);
                }
            })
            .on("mouseup", d => {
                mousedown1 = false;
                let value1 = this.xScale.invert(d.offsetX);
                let value2 = this.xScale.invert(+d3.select("#timeSelectCircle2").attr("cx"));
                this.lineGrapher.zoom(Math.min(value1, value2), Math.max(value1, value2));

            });

        let mousedown2 = false;
        this.parent.append("circle")
            .attr("id", "timeSelectCircle2")
            .attr("cx", this.width)
            .attr("cy", this.height/2)
            .attr("r", 10)
            .attr("stroke", "darkgray")
            .attr("fill", "darkgray")
            .attr("fill-opacity", 0.5)
            .on("mousedown", d => {
                mousedown2 = true;
            })
            .on("mousemove", function(d) {
                if(mousedown2){
                    d3.select(this).attr("cx", d.offsetX);
                }
            })
            .on("mouseout", function(d) {
                if(mousedown2){
                    d3.select(this).attr("cx", d.offsetX);
                }
            })
            .on("mouseup", d => {
                mousedown2 = false;
                let value1 = this.xScale.invert(d.offsetX);
                let value2 = this.xScale.invert(+d3.select("#timeSelectCircle1").attr("cx"));
                this.lineGrapher.zoom(Math.min(value1, value2), Math.max(value1, value2));

            });

        //Add the clip path to everything in the SVG
        this.parent.selectAll("*")
            .attr("clip-path", "url(#clip)");
    }

    setCircleLocations(c1, c2){
        //This method sets the location of the circles to a specified center 1 and center 2

        d3.select("#timeSelectCircle1").attr("cx", this.xScale(c1));
        d3.select("#timeSelectCircle2").attr("cx", this.xScale(c2));
    }
}