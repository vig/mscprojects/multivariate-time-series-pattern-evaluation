import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";
import {TruthMatching} from "./truthMatching.js";

export class GlyphCharter{

    parent;
    width;
    height;
    timeSeries;
    methodList;
    groundTruthList;

    truthMatchArr;
    falseTruthByMethod;
    chronAscTruthMatchArr;

    lineGrapher;
    miniMapper;

    xScale;
    yScale;
    timeSeriesScale;

    confidenceCutoff = 0.5;

    currentPage = 1;
    glyphsPerPage = 15;

    zoomed = false;

    sortStyle = "chronAsc";

    constructor(parent, width, height, timeSeries, methodList, groundTruthList, lineGrapher, miniMapper){
        this.parent = parent;
        this.width = width;
        this.height = height;
        this.timeSeries = timeSeries;
        this.methodList = methodList;
        this.groundTruthList = groundTruthList;
        this.lineGrapher = lineGrapher;
        this.miniMapper = miniMapper;

        this.truthMatchArr = [];
        this.groundTruthList.forEach((truth, index) => {
            this.truthMatchArr.push(new TruthMatching(truth, this.methodList, index));
        })

        //Loop over all methods and their patterns to find which ones do not match any ground truths
        this.falseTruthByMethod = new Map();
        this.methodList.forEach((method) => {
            let falsePos = [];
            method.patternList.forEach((pattern) => {
                let found = false;
                this.groundTruthList.forEach((truth) => {
                    if(pattern.isMatch(truth)){
                        found = true;
                    }
                })
                if(!found){
                    falsePos.push(pattern);
                }
            })
            if(this.falseTruthByMethod.has(method)){
                this.falseTruthByMethod.set(method, this.falseTruthByMethod.get(method).concat(falsePos));
            } else {
                this.falseTruthByMethod.set(method, falsePos);
            }
        })

        //TODO think of way to add false positives into the matchmap or w/e

        //Get variables for the creation of xScale
        let largestGlyphSize = this.getLargestGlyphSize();

        let glyphMaxWidth = this.width / this.glyphsPerPage;
        //Define scales initially
        this.xScale = d3.scaleLinear()
            .domain([0,largestGlyphSize])
            .range([0, glyphMaxWidth]);

        this.yScale = d3.scaleLinear()
            .domain([0, this.glyphsPerPage])
            .range([0, this.height]);

        //Get variables for timesteps in series
        let timeMin = d3.min(timeSeries, d=> {
            return d.time;
        })
        let timeMax = d3.max(timeSeries, d=> {
            return d.time;
        })
        //Define scale that transforms time series time to time steps
        this.timeSeriesScale = d3.scaleLinear()
            .domain([timeMin, timeMax])
            .range([0, timeSeries.length]);


        //Calculate needed pages for all glyphs
        this.pages = Math.ceil(this.truthMatchArr.length / this.glyphsPerPage);

        this.sortPatterns("chronAsc");
        this.chronAscTruthMatchArr = [];
        this.truthMatchArr.forEach((truthMatching, index) => {
            this.chronAscTruthMatchArr.push(new TruthMatching(truthMatching.truth, this.methodList, index));
        })

        this.initGlyphsPerPageSelect();
        this.initPagination();

        this.draw();
    }

    draw(){

        d3.selectAll("#glyphChartTarget > svg > g > *").remove();

        let glyphMaxWidth = this.width / this.glyphsPerPage;
        let xPadding = 0;

        //Loops over ground Truths
        this.truthMatchArr.forEach((truthMatching, index) => {


            //Only draw when index is within the range of the current page
            let firstIndex = this.glyphsPerPage * (this.currentPage - 1);
            let lastIndex = (this.glyphsPerPage * this.currentPage) - 1;

            if(firstIndex <= index && index <= lastIndex){

                let smallestStart = truthMatching.getSmallestStart();
                let truth = truthMatching.truth;

                //Account for the shift of indices when selecting a next page (otherwise glyphs are drawn off graph)
                let newIndex = index - firstIndex;

                this.parent.append("rect")
                    .attr("x", ((this.xScale(truth.start - smallestStart)) + (glyphMaxWidth * newIndex)) +
                        (xPadding * newIndex))
                    .attr("y", 0)
                    .attr("width", this.xScale(truth.end) - this.xScale(truth.start))
                    .attr("height", this.height)
                    .attr("fill", "lightgrey")
                    .on("click", d => {
                        const PADDING = 30;

                        if(!this.zoomed){
                            this.lineGrapher.zoom(truthMatching.getSmallestStart() - this.timeSeriesScale.invert(PADDING)
                                , truthMatching.getLargestEnd() + this.timeSeriesScale.invert(PADDING));
                            d3.select(d.srcElement).attr("fill", "dimgray").attr("class", "currentSelection");

                            this.miniMapper.setCircleLocations(truthMatching.getSmallestStart() - this.timeSeriesScale.invert(PADDING)
                            , truthMatching.getLargestEnd() + this.timeSeriesScale.invert(PADDING));

                            this.zoomed = !this.zoomed;
                        } else {
                            d3.select(".currentSelection").attr("fill", "lightgrey").attr("class", null);
                            let startTime = d3.min(this.timeSeries, d=> {
                                return d.time;
                            });
                            let endTime = d3.max(this.timeSeries, d=> {
                                return d.time;
                            });
                            this.lineGrapher.zoom(startTime, endTime);
                            this.miniMapper.setCircleLocations(startTime, endTime);
                            this.zoomed = !this.zoomed;
                        }
                    });

                this.parent.append("line")
                    .attr("x1", ((this.xScale(truth.start - smallestStart)) + (glyphMaxWidth * newIndex)) +
                        (xPadding * newIndex) + this.xScale(truth.end) - this.xScale(truth.start))
                    .attr("x2", ((this.xScale(truth.start - smallestStart)) + (glyphMaxWidth * newIndex)) +
                        (xPadding * newIndex) + this.xScale(truth.end) - this.xScale(truth.start))
                    .attr("y1", 0)
                    .attr("y2", this.height)
                    .attr("stroke", "grey")
                    .attr("stroke-dasharray", 6);

                this.parent.append("line")
                    .attr("x1", ((this.xScale(truth.start - smallestStart)) + (glyphMaxWidth * newIndex)) +
                        (xPadding * newIndex))
                    .attr("x2", ((this.xScale(truth.start - smallestStart)) + (glyphMaxWidth * newIndex)) +
                        (xPadding * newIndex))
                    .attr("y1", this.height)
                    .attr("y2", 0)
                    .attr("stroke", "grey")
                    .attr("stroke-dasharray", 6);

                this.parent.append("text")
                    .attr("x", 2 + ((this.xScale(truth.start - smallestStart)) + (glyphMaxWidth * newIndex)) +
                        (xPadding * newIndex))
                    .attr("y", 10)
                    .text(truthMatching.id);

                truthMatching.methodMatchMap.keys().forEach((method, i) => {

                    let methodMatches = truthMatching.methodMatchMap.get(method);

                    methodMatches.forEach((match) => {

                        if(match.confidence > this.confidenceCutoff){
                            this.parent.append("rect")
                                .attr("x", (this.xScale(match.start - smallestStart) + (glyphMaxWidth * newIndex)) +
                                    (xPadding * newIndex))
                                .attr("y", 20 + (20 / truthMatching.getMethodAmount()) * i)
                                .attr("width", this.xScale(match.end - match.start))
                                .attr("height", 20/truthMatching.getMethodAmount())
                                .attr("opacity", 1)
                                .attr("fill", method.color);
                        }

                    })
                })
            }
        })
    }

    getLargestGlyphSize(){
        let result = 0;

        this.truthMatchArr.forEach((truthMatching) => {
            if(truthMatching.getLargestSize() > result) result = truthMatching.getLargestSize();
        })

        return result;
    }

    sortPatterns(sortStyle){
        //First basic sorting, currently only on the data of the ground truths
        this.sortStyle = sortStyle;
        this.truthMatchArr = this.truthMatchArr.sort( (a,b) => {
            switch(this.sortStyle){
                case "chronAsc":
                    return a.truth.start - b.truth.start;
                case "chronDesc":
                    return b.truth.start - a.truth.start;
                case "confAsc":
                    if(a.getAverageConfidence() === null) {
                        return 1;
                    } else if(b.getAverageConfidence() === null){
                        return -1;
                    } else {
                        return a.getAverageConfidence() - b.getAverageConfidence();
                    }
                case "confDesc":
                    if(a.getAverageConfidence() === null) {
                        return 1;
                    } else if(b.getAverageConfidence() === null){
                        return -1;
                    } else {
                        return b.getAverageConfidence() - a.getAverageConfidence();
                    }
                case "highConfAsc":
                    if(a.getHighestConfidence() === null){
                        return 1;
                    } else if (b.getHighestConfidence() === null){
                        return -1;
                    } else {
                        return b.getHighestConfidence() - a.getHighestConfidence();
                    }
                case "highConfDesc":
                    if(a.getHighestConfidence() === null){
                        return -1;
                    } else if (b.getHighestConfidence() === null){
                        return 1;
                    } else {
                        return a.getHighestConfidence() - b.getHighestConfidence();
                    }
                case "lowConfAsc":
                    if(a.getLowestConfidence() === null){
                        return 1;
                    } else if (b.getLowestConfidence() === null){
                        return -1;
                    } else {
                        return b.getHighestConfidence() - a.getHighestConfidence();
                    }
                case "lowConfDesc":
                    if(a.getLowestConfidence() === null){
                        return -1;
                    } else if (b.getLowestConfidence() === null){
                        return 1;
                    } else {
                        return a.getLowestConfidence() - b.getLowestConfidence();
                    }
                case "discAsc":
                    if(a.getDiscrepancy() === null){
                        return 1;
                    } else if (b.getDiscrepancy() === null){
                        return -1;
                    } else {
                        return b.getDiscrepancy() - a.getDiscrepancy();
                    }
                case "discDesc":
                    if(a.getDiscrepancy() === null){
                        return -1;
                    } else if (b.getDiscrepancy() === null){
                        return 1;
                    } else {
                        return a.getDiscrepancy() - b.getDiscrepancy();
                    }
            }
        })

        this.draw();
    }

    initGlyphsPerPageSelect(){
        let max = this.truthMatchArr.length;
        console.log(max);
        for(let i = 1; i<max; i++){
            d3.select("#glyphsPerPageSelect").append("option")
                .attr("name", i)
                .attr("value", i)
                .text(i);
        }
        d3.select("#glyphsPerPageSelect").on("change", d=> {
            this.updateGlyphsPerPage(d3.select("#glyphsPerPageSelect").property("value"));
        })
    }

    updateGlyphsPerPage(newAmount){
        this.glyphsPerPage = newAmount;
        this.draw();
    }

    alterCutoff(newCutoff){
        this.confidenceCutoff = newCutoff;
        this.draw();
    }

    initPagination(){
        for(let i=1; i<=this.pages; i++){
            d3.select("#paginationTarget").append("li")
                .attr("id", "paginationNumber" + i)
                .attr("class",  "pagination-link mt-0")
                .text(i)
                .on("click", d =>{
                    this.changePage(i);
                });
        }
        //Add functionality to the previous and next buttons
        d3.select("#paginationNext").on("click", d =>{
            if(!(d3.select(d.srcElement).attr("disabled") === "disabled")) {
                this.changePage(this.currentPage + 1);
            }
        })
        d3.select("#paginationPrevious").on("click", d =>{
            if(!(d3.select(d.srcElement).attr("disabled") === "disabled")){
                this.changePage(this.currentPage  - 1);
            }
        })

        //Set current page to page 1 and disable previous
        d3.select("#paginationNumber1").attr("class", "pagination-link mt-0 is-current");
        d3.select("#paginationPrevious").attr("disabled", "disabled");
    }

    changePage(newPage){
        //Deselect old current page link
        d3.select("#paginationNumber" + this.currentPage).attr("class", "pagination-link mt-0");

        this.currentPage = newPage;

        //Set new active page link
        d3.select("#paginationNumber" + this.currentPage).attr("class", "pagination-link mt-0 is-current")

        //Potentially disable next and previous buttons
        if(this.currentPage === this.pages){
            d3.select("#paginationNext").attr("disabled", "disabled");
        } else {
            d3.select("#paginationNext").attr("disabled", null);
        }

        if(this.currentPage === 1){
            d3.select("#paginationPrevious").attr("disabled", "disabled");
        } else {
            d3.select("#paginationPrevious").attr("disabled", null);
        }

        this.draw();
    }

}