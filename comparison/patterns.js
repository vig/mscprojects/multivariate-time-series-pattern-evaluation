class Pattern {
    start;
    end;

    constructor(start, end){
        this.start = start;
        this.end = end;
    }
}

export class FoundPattern extends Pattern{

    confidence;

    constructor(start, end, confidence){
        super(start, end);
        this.confidence = confidence;
    }

    isMatch(pattern) {
        //Match two patterns.
        //Used cases: Found start or end within truth, truth covers entire pattern, pattern covers entire truth
        if((pattern.start <= this.start) && (pattern.end >= this.start)){
            return true;
        } else if ((pattern.start <= this.end) && (pattern.end >= this.end)) {
            return true;
        } else if ((this.start < pattern.start) && (this.end > pattern.end)) {
            return true;
        } else if ((this.start > pattern.start) && (this.end < pattern.end)) {
            return true;
        } else {
            return false;
        }
    }

}

export class GroundTruth extends Pattern {

    constructor(start, end) {
        super(start, end);
    }

    isMatch(pattern) {
        //Match two patterns.
        //Used cases: Found start or end within truth, truth covers entire pattern, pattern covers entire truth
        if((this.start <= pattern.start) && (this.end >= pattern.start)){
            return true;
        } else if ((this.start <= pattern.end) && (this.end >= pattern.end)) {
            return true;
        } else if ((pattern.start < this.start) && (pattern.end > this.end)) {
            return true;
        } else if ((pattern.start > this.start) && (pattern.end < this.end)) {
            return true;
        } else {
            return false;
        }
    }

}