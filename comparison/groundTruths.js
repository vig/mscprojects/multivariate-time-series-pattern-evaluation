export class GroundTruthlist extends Array {

    constructor(){
        super();
    }

    //Checks all patterns in method if they
    getMatchesByMethod(methods){
        let result = new Map();
        methods.forEach((method) => {
            let methodMatches = new Map();
            this.forEach((truth) => {
                let matchingPatterns = [];
                method.patternList.forEach((found) => {
                    if(truth.isMatch(found)){
                        matchingPatterns.push(found);
                    }
                })
                methodMatches.set(truth, matchingPatterns);
            })
            result.set(method, methodMatches);
        })
        return result;
    }

    //Method returns the following: ({} = map, [] = array)
    // {truth, {method,[matches]}}
    getMatchesByTruth(methods){
        let result = new Map();
        this.forEach((truth) => {
            let methodMatches = new Map();
            methods.forEach((method) => {
                let matchingPatterns = [];
                method.patternList.forEach((pattern) => {
                    if(truth.isMatch(pattern)){
                        matchingPatterns.push(pattern);
                    }
                })
                methodMatches.set(method, matchingPatterns);
            })
            result.set(truth, methodMatches);
        })
        return result;
    }
}