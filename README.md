# Multivariate Time Series Pattern Evaluation

Visualization tool to evaluate time series pattern search algorithms results. Created for University of Utrecht Master of Business Informatics thesis, "Visual Analytics Guided Exploration and Assessment of Time Series Pattern Search Algorithms". <br>
Created in collaboration with IAV GmbH.

## Tool modules

### Algorithm Explorer

This module allow users to explore the results of a single pattern search algorithm.
On startup, you are greeted with file upload options. Please make sure to upload the right files (formatted correctly) in the right place, otherwise the tool might not function as expected. To skip uploading files, use the "Use demo files to skip file upload."-button. This button uses data, included with the code, to allow for evaluation of the tool without the need to upload the correct data files.

After uploading files, the user is greeted by the three main elements of this module. On the left, the glyph chart shows the relative position of found patterns compared to their matching predetermined ground truths. On the top right, the user is able to filter the data in the glyph chart and line graph according to confidence or score. On the bottom right, the line graph shows the entire time series, including the location of found patterns and ground truths. All visualizations are fully interactive, in order to provide the proper context to users.

### Algorithm Comparison

The Algorithm Comparison module is in a more bare-bones state. Due to issues uploading files, the release version only uses data included with the code.

Set-up in a similar way as the Algorithm Exploration module, the Algorithm Comparison module uses a large glyph chart, this time at the top of the page, to show the relative locations of found patterns (grouped by the algorithm that created them) and their matching ground truths. Below the glyph chart, the user is again able to filter data according to confidence and gain extra context and information in a line chart showing the entire time series.

## Installation Guide

While the tool does require the D3.js library to function, all files required are included or imported directly from the D3 website.
The tool can be executed by any javascript capable HTML-server, hosted locally or remotely. Both local and remote hosting were tested during the implementation of the tool.

## Technical Requirements

Javascript library: D3.js, version 7. <br>
CSS Framework: Bulma, files included in Resources folder.

## Technical Support
When unexpected issues occur within the tool, don't hesitate to contact the main developer on m.sneekes@students.uu.nl.